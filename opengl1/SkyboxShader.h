#include "ShaderProgram.h"
#include "Maths.h"
#include "Camera.h"

#ifndef SKYBOX_SHADER_H
#define SKYBOX_SHADER_H

class SkyboxShader : public ShaderProgram
{
public:
	explicit SkyboxShader() : ShaderProgram(_VERTEX_FILE, _FRAGMENT_FILE)
	{
		getAllUniformLocations();
		bindAttributes();
	};
	~SkyboxShader();

	virtual void bindAttributes() override;
	virtual void getAllUniformLocations() override;

	void loadProjectionMatrix(glm::mat4 &p_matrix);
	void loadViewMatrix(Camera &p_camera);
	void loadTransformMatrix(glm::mat4 &p_matrix);

private:
	static const char* _VERTEX_FILE;
	static const char* _FRAGMENT_FILE;

	int _location_projectionMatrix;
	int _location_viewMatrix;
	int _location_transformMatrix;
};


#endif SKYBOX_SHADER_H