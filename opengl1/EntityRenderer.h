#include "Utility.h"
#include "RawModel.h"
#include "TexturedModel.h"
#include "Entity.h"
#include "StaticShader.h"
#include "Maths.h"
#include "DisplayManager.h"
#include "Loader.h"
#include <map>

#ifndef ENTITY_RENDERER_H
#define ENTITY_RENDERER_H

class EntityRenderer
{
public:
	explicit EntityRenderer(StaticShader* p_shader, glm::mat4 p_projectionMatrix, Loader * p_loader) : _shader(p_shader), _loader(p_loader)
	{
		p_shader->start();
		p_shader->loadProjectionMatrix(p_projectionMatrix);
		p_shader->stop();
	};
	~EntityRenderer();

	static void enableBackfaceCulling();
	static void disableBackfaceCulling();

	void render(std::map <int, std::pair<TexturedModel, std::vector<Entity>> > p_entities);
	void renderRaw(RawModel p_model);
	void renderTextured(TexturedModel p_texturedModel);

private:
	static const int _INSTANCE_DATA_LENGTH = 16;

	StaticShader *_shader;
	Loader * _loader;
	GLuint _instanceVBO;
	bool _instanceVBOCreated = false;

	void prepareTexturedModel(TexturedModel p_model);
	void prepareInstancedVBO(std::map <int, std::pair<TexturedModel, std::vector<Entity>> > p_entities);
	void unbindTexturedModel();
	void prepareInstance(Entity p_entity); 
	void storeMatrixData(glm::mat4 p_mat, std::vector<float> & p_vboData);
};

#endif ENTITY_RENDERER_H