#include "Utility.h"

#ifndef MODEL_TEXTURE_H
#define MODEL_TEXTURE_H

class ModelTexture
{
public:
	explicit ModelTexture() : ModelTexture(-1) 
	{
		_shineDamper = 1.0f;
		_reflectivity = 0.0f;
	};
	explicit ModelTexture(int p_id) : _textureID(p_id) {};
	~ModelTexture();

	int getTextureID() const;
	float getShineDamper() const;
	float getReflectivity() const;
	bool getHasTransparency() const;
	bool getUseFakeLighting() const;
	void setShineDamper(float p_shineDamper);
	void setReflectivity(float p_reflectivity);
	void setHasTransparency(bool p_hasTransparency);
	void setUseFakeLighting(bool p_useFakeLighting);

private:
	int _textureID;
	float _shineDamper;
	float _reflectivity;
	bool _hasTransparency = false;
	bool _useFakeLighting = false;
};

#endif MODEL_TEXTURE_H