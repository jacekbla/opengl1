#include "ModelTexture.h"



ModelTexture::~ModelTexture()
{
}

int ModelTexture::getTextureID() const
{
	return _textureID;
}

float ModelTexture::getShineDamper() const
{
	return _shineDamper;
}

float ModelTexture::getReflectivity() const
{
	return _reflectivity;
}

bool ModelTexture::getHasTransparency() const
{
	return _hasTransparency;
}

bool ModelTexture::getUseFakeLighting() const
{
	return _useFakeLighting;
}

void ModelTexture::setHasTransparency(bool p_hasTransparency)
{
	_hasTransparency = p_hasTransparency;
}

void ModelTexture::setUseFakeLighting(bool p_useFakeLighting)
{
	_useFakeLighting = p_useFakeLighting;
}

void ModelTexture::setShineDamper(float p_shineDamper)
{
	_shineDamper = p_shineDamper;
}

void ModelTexture::setReflectivity(float p_reflectivity)
{
	_reflectivity = p_reflectivity;
}
