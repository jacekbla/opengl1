#version 430 
#define MAX_LIGHTS 16

in vec2 vTexCoordsFrag;
in vec3 vSurfaceNormal;
in vec3 vToLightVector[MAX_LIGHTS];
in vec3 vToCameraVector;
in float visibility;

out vec4 fColor;

uniform sampler2D backgroundTexture;
uniform sampler2D rColor;
uniform sampler2D gColor;
uniform sampler2D bColor;
uniform sampler2D blendMap;

uniform vec3 lightColor[MAX_LIGHTS];
uniform vec3 attenuation[MAX_LIGHTS];
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;
uniform int lightCount;

void main()
{
	vec4 blendMapColor = texture(blendMap, vTexCoordsFrag);

	float backTextureAmount = 1 - (blendMapColor.r + blendMapColor.g + blendMapColor.b);
	vec2 tiledCoords = vTexCoordsFrag * 50.0;
	vec4 backgroundTextureColor = texture(backgroundTexture, tiledCoords) * backTextureAmount;
	vec4 rTextureColor = texture(rColor, tiledCoords) * blendMapColor.r;
	vec4 gTextureColor = texture(gColor, tiledCoords) * blendMapColor.g;
	vec4 bTextureColor = texture(bColor, tiledCoords) * blendMapColor.b;

	vec4 totalColor = backgroundTextureColor + rTextureColor + gTextureColor + bTextureColor;

    vec3 unitNormal = normalize(vSurfaceNormal);
    vec3 unitToCameraVector = normalize(vToCameraVector);
	
	vec3 totalDiffuse = vec3(0.0);
	vec3 totalSpecular = vec3(0.0);

	for(int i = 0; i < lightCount; i++)
	{
		float distance = length(vToLightVector[i]);
		float attFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
		vec3 unitToLightVector = normalize(vToLightVector[i]);
		float nDotl = dot(unitNormal, unitToLightVector);
		float brightness = max(nDotl, 0.0);
		vec3 lightDirection = -unitToLightVector;
		vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);
		float specularFactor = dot(reflectedLightDirection, unitToCameraVector);
		specularFactor = max(specularFactor, 0.0);
		float dampedFactor = pow(specularFactor, shineDamper);
		totalDiffuse = totalDiffuse + (brightness * lightColor[i]) / attFactor;
		totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColor[i]) / attFactor;
	}
	totalDiffuse = max(totalDiffuse, 0.1);

    fColor = vec4(totalDiffuse, 1.0) * totalColor + vec4(totalSpecular, 0.0);

	//fog effect
	fColor = mix(vec4(skyColor, 1.0), fColor, visibility);
}