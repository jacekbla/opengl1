#include "RawModel.h"
#include "Utility.h"
#include "stb_image.hpp"

#ifndef LOADER_H
#define LOADER_H

class Loader
{
public:
	explicit Loader();
	~Loader();

	RawModel loadToVAO(std::vector<float> p_positions, std::vector<float> p_colors, std::vector<float> p_texCoords, std::vector<float> p_normals, std::vector<unsigned int> p_indices);
	RawModel loadToVAO(std::vector<float> p_positions, std::vector<float> p_texCoords, std::vector<float> p_normals, std::vector<unsigned int> p_indices);
	RawModel loadToVAO(std::vector<float> p_positions, int p_dimensions); 
	RawModel loadToVAO_sphere(std::vector<float> p_positions, std::vector<float> p_texCoords, std::vector<float> p_normals,
		std::vector<unsigned int> p_indices, std::pair<glm::vec3, float> p_sphere);
	RawModel loadWaterToVAO(std::vector<float> p_positions, std::vector<float> p_indicators);
	void cleanUp();
	GLuint createrEmptyVBO(int p_floatCount);
	void addInstancedAttribute(int p_vao, int p_vbo, int p_attrib, int p_dataSize, int p_instancedDataLength, int p_offset);
	void updateVBO(int p_vbo, std::vector<float> & p_data);
	GLuint loadTexture(std::string p_fileName);
	GLuint loadPNGTexture(const char* p_fileName);
	GLuint loadPNGTextureNoAlpha(const char* p_fileName);
	GLuint loadCubeMap(std::vector<const char *> p_textureFiles);
	GLuint loadCubeMapNoAlpha(std::vector<const char*> p_textureFiles);

private:
	int createVAO();
	void storeDataInAtrributeList(int p_attributeNumber, int p_coordinateSize, std::vector<float> p_data);
	void storeIntDataInAtrributeList(int p_attributeNumber, int p_coordinateSize, std::vector<int> p_data);
	void unbindVAO();
	void bindIndicesBuffer(std::vector<unsigned int> p_indices);


	std::vector<GLuint> _vaos;
	std::vector<GLuint> _vbos;
	std::vector<GLuint> _textures;
};

#endif LOADER_H