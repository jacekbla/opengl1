#include "Utility.h"
#include "DisplayManager.h"

#ifndef CAMERA_H
#define CAMERA_H

class Camera
{
public:
	explicit Camera()
	{ };
	~Camera();

	glm::vec3 getPosition() const;
	float getPitch() const;
	float getYaw() const;
	float getRoll() const;
	void setPosition(glm::vec3 p_position);

	void invertPitch();
	void move(GLFWwindow* p_window);
	void moveForward();
	void moveUp();
	void turnLeft();
	void turnRight();
	void turnUp();
	void turnDown();

private:
	glm::vec3 _position = glm::vec3(0.0f, 5.0f, 0.0f);
	float _pitch = 5.0f;
	float _yaw = 0.0f;
	float _roll = 0.0f;

	const float _MOUSE_ANGLE_SENSITIVITY = 0.01f;
	const float _KEYBOARD_ANGLE_SENSITIVITY = 0.01f;
	const float _MOVE_SENSITIVITY = 0.04f;

	double _currMouseX;
	double _currMouseY;
};


#endif CAMERA_H