#version 430 
#define MAX_LIGHTS 16

in vec2 vTexCoordsFrag;
in vec3 vSurfaceNormal;
in vec3 vToLightVector[MAX_LIGHTS];		//4 - max lights affecting an object
in vec3 vToCameraVector;
in float visibility;

out vec4 fColor;

uniform sampler2D textureSampler;
uniform vec3 lightColor[MAX_LIGHTS];
uniform vec3 attenuation[MAX_LIGHTS];
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;
uniform int lightCount;

void main()
{
    vec3 unitNormal = normalize(vSurfaceNormal);
    vec3 unitToCameraVector = normalize(vToCameraVector);

	vec3 totalDiffuse = vec3(0.0);
	vec3 totalSpecular = vec3(0.0);

	for(int i = 0; i < lightCount; i++)
	{
		float distance = length(vToLightVector[i]);
		float attFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
		vec3 unitToLightVector = normalize(vToLightVector[i]);
		float nDotl = dot(unitNormal, unitToLightVector);
		float brightness = max(nDotl, 0.0);
		vec3 lightDirection = -unitToLightVector;
		vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);
		float specularFactor = dot(reflectedLightDirection, unitToCameraVector);
		specularFactor = max(specularFactor, 0.0);
		float dampedFactor = pow(specularFactor, shineDamper);
		totalDiffuse = totalDiffuse + (brightness * lightColor[i]) / attFactor;
		totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColor[i]) / attFactor;
	}
	totalDiffuse = max(totalDiffuse, 0.1);

	vec4 textureColor = texture(textureSampler, vTexCoordsFrag);
	if(textureColor.a < 0.5)
	{
		discard;
	}

    fColor = vec4(totalDiffuse, 1.0) * textureColor + vec4(totalSpecular, 1.0);

	//fog effect
	fColor = mix(vec4(skyColor, 1.0), fColor, visibility);
}
