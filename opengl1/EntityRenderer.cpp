#include "EntityRenderer.h"


EntityRenderer::~EntityRenderer()
{
	delete _shader;
	delete _loader;
}

void EntityRenderer::enableBackfaceCulling()
{
	//backface culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void EntityRenderer::disableBackfaceCulling()
{
	glDisable(GL_CULL_FACE);
}

void EntityRenderer::render(std::map <int, std::pair<TexturedModel, std::vector<Entity>> > p_entities)
{
	//instanced rendering
	if (!_instanceVBOCreated)
	{
		prepareInstancedVBO(p_entities);
	}
	for (std::map <int, std::pair<TexturedModel, std::vector<Entity>> >::value_type& val : p_entities)
	{
		TexturedModel model = val.second.first;
		prepareTexturedModel(model);
		std::vector<Entity> batch = val.second.second;


		std::vector<float> vboData;
		for (Entity e : batch)
		{
			glm::mat4 transformMatrix = Maths::createTransformMatrix(e.getPosition(), e.getRotX(), e.getRotY(), e.getRotZ(), e.getScale());
			storeMatrixData(transformMatrix, vboData);
		}
		_loader->updateVBO(_instanceVBO, vboData);
		glDrawElementsInstanced(GL_TRIANGLES, model.getRawModel().getVertexCount(), GL_UNSIGNED_INT, 0, batch.size());
		
		unbindTexturedModel();
	}

	//not instanced
	/*for (std::map <int, std::pair<TexturedModel, std::vector<Entity>> >::value_type& val : p_entities)
	{
		TexturedModel model = val.second.first;
		prepareTexturedModel(model);
		std::vector<Entity> batch = val.second.second;
		for (Entity e : batch)
		{
			prepareInstance(e);
			glDrawElements(GL_TRIANGLES, model.getRawModel().getVertexCount(), GL_UNSIGNED_INT, 0);
		}
		unbindTexturedModel();
	}*/
}

void EntityRenderer::renderRaw(RawModel p_model)
{
	glBindVertexArray(p_model.getVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glDrawElements(GL_TRIANGLES, p_model.getVertexCount(), GL_UNSIGNED_INT, 0);

	//glutSwapBuffers();
	//glutPostRedisplay();

	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}

void EntityRenderer::renderTextured(TexturedModel p_texturedModel)
{
	RawModel model = p_texturedModel.getRawModel();
	glBindVertexArray(model.getVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, p_texturedModel.getTexture().getTextureID());
	glDrawElements(GL_TRIANGLES, model.getVertexCount(), GL_UNSIGNED_INT, 0);

	//glutSwapBuffers();
	//glutPostRedisplay();

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void EntityRenderer::prepareTexturedModel(TexturedModel p_model)
{
	RawModel rawModel = p_model.getRawModel();
	glBindVertexArray(rawModel.getVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);

	ModelTexture texture = p_model.getTexture();
	if (texture.getHasTransparency())
	{
		EntityRenderer::disableBackfaceCulling();
	}

	_shader->loadUseFakeLighting(texture.getHasTransparency());
	_shader->loadShineVariables(texture.getShineDamper(), texture.getReflectivity());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
}

void EntityRenderer::prepareInstancedVBO(std::map <int, std::pair<TexturedModel, std::vector<Entity>> > p_entities)
{
	int maxSize = 0;
	for (std::map <int, std::pair<TexturedModel, std::vector<Entity>> >::value_type& val : p_entities)
	{
		int size = val.second.second.size();
		if (size > maxSize)
		{
			maxSize = size;
		}
	}
	_instanceVBO = _loader->createrEmptyVBO(_INSTANCE_DATA_LENGTH * maxSize);

	for (std::map <int, std::pair<TexturedModel, std::vector<Entity>> >::value_type& val : p_entities)
	{
		TexturedModel model = val.second.first;
		prepareTexturedModel(model);
		int vao = val.second.first.getRawModel().getVaoID();
		_loader->addInstancedAttribute(vao, _instanceVBO, 3, sizeof(float), _INSTANCE_DATA_LENGTH, 0);
		_loader->addInstancedAttribute(vao, _instanceVBO, 4, sizeof(float), _INSTANCE_DATA_LENGTH, 4);
		_loader->addInstancedAttribute(vao, _instanceVBO, 5, sizeof(float), _INSTANCE_DATA_LENGTH, 8);
		_loader->addInstancedAttribute(vao, _instanceVBO, 6, sizeof(float), _INSTANCE_DATA_LENGTH, 12);
		unbindTexturedModel();
	}
	_instanceVBOCreated = true;
}

void EntityRenderer::unbindTexturedModel()
{
	EntityRenderer::enableBackfaceCulling();
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glDisableVertexAttribArray(5);
	glDisableVertexAttribArray(6);
	glBindVertexArray(0);
}

void EntityRenderer::prepareInstance(Entity p_entity)
{
	glm::mat4 transformMatrix = Maths::createTransformMatrix(p_entity.getPosition(), p_entity.getRotX(), p_entity.getRotY(), p_entity.getRotZ(), p_entity.getScale());
	_shader->loadTransformMatrix(transformMatrix);
}

void EntityRenderer::storeMatrixData(glm::mat4 p_mat, std::vector<float>& p_vboData)
{
	p_vboData.push_back(p_mat[0][0]);
	p_vboData.push_back(p_mat[0][1]);
	p_vboData.push_back(p_mat[0][2]);
	p_vboData.push_back(p_mat[0][3]);
	p_vboData.push_back(p_mat[1][0]);
	p_vboData.push_back(p_mat[1][1]);
	p_vboData.push_back(p_mat[1][2]);
	p_vboData.push_back(p_mat[1][3]);
	p_vboData.push_back(p_mat[2][0]);
	p_vboData.push_back(p_mat[2][1]);
	p_vboData.push_back(p_mat[2][2]);
	p_vboData.push_back(p_mat[2][3]);
	p_vboData.push_back(p_mat[3][0]);
	p_vboData.push_back(p_mat[3][1]);
	p_vboData.push_back(p_mat[3][2]);
	p_vboData.push_back(p_mat[3][3]);
}
