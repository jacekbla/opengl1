#include "ShaderProgram.h"
#include "Camera.h"
#include "Maths.h"
#include "Light.h"

#ifndef TERRAIN_SHADER_H
#define TERRAIN_SHADER_H

class TerrainShader : public ShaderProgram
{
public:
	explicit TerrainShader() : ShaderProgram(_VERTEX_FILE, _FRAGMENT_FILE)
	{
		getAllUniformLocations();
		bindAttributes();
	};
	~TerrainShader();

	virtual void bindAttributes() override;
	virtual void getAllUniformLocations() override;

	void loadTransformMatrix(glm::mat4 &p_matrix);
	void loadProjectionMatrix(glm::mat4 &p_matrix);
	void loadViewMatrix(Camera &p_camera);
	void loadLights(std::vector<Light> &p_lights);
	void loadShineVariables(float p_damper, float p_reflectivity);
	void loadSkyColor(float p_r, float p_g, float p_b);
	void loadClipPlane(glm::fvec4 p_plane);
	void connectTextureUnits();

private:
	static const char* _VERTEX_FILE;
	static const char* _FRAGMENT_FILE;
	static const int _MAX_LIGHTS = 16;

	int _location_transformMatrix;
	int _location_projectionMatrix;
	int _location_viewMatrix;
	int _location_lightPosition[_MAX_LIGHTS];
	int _location_lightColor[_MAX_LIGHTS];
	int _location_attenuation[_MAX_LIGHTS];
	int _location_shineDamper;
	int _location_reflectivity;
	int _location_skyColor;
	int _location_backgroundColor;
	int _location_rColor;
	int _location_gColor;
	int _location_bColor;
	int _location_blendMap;
	int _location_plane;
	int _location_lightCount;
};


#endif TERRAIN_SHADER_H