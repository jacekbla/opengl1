#include "SkyboxShader.h"

const char* SkyboxShader::_VERTEX_FILE = "skyboxVert.glsl";
const char* SkyboxShader::_FRAGMENT_FILE = "skyboxFrag.glsl";



SkyboxShader::~SkyboxShader()
{
}

void SkyboxShader::bindAttributes()
{
	ShaderProgram::bindAttribute(0, "position");
}

void SkyboxShader::getAllUniformLocations()
{
	_location_projectionMatrix = ShaderProgram::getUniformLocation("projectionMatrix");
	_location_viewMatrix = ShaderProgram::getUniformLocation("viewMatrix");
	_location_transformMatrix = ShaderProgram::getUniformLocation("transformMatrix");
}

void SkyboxShader::loadProjectionMatrix(glm::mat4 &p_matrix)
{
	ShaderProgram::loadMatrix(_location_projectionMatrix, p_matrix);
}

void SkyboxShader::loadViewMatrix(Camera &p_camera)
{
	glm::mat4 viewMatrix = Maths::createViewMatrix(p_camera);
	viewMatrix[3][0] = 0.0f;
	viewMatrix[3][1] = 0.0f;
	viewMatrix[3][2] = 0.0f;
	ShaderProgram::loadMatrix(_location_viewMatrix, viewMatrix);
}

void SkyboxShader::loadTransformMatrix(glm::mat4 &p_matrix)
{
	ShaderProgram::loadMatrix(_location_transformMatrix, p_matrix);
}
