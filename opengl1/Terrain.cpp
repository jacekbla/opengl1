#include "Terrain.h"

const float Terrain::_SIZE = 100.0f;
const float Terrain::_MAX_HEIGHT = 15.0f;
const float Terrain::_MIN_HEIGHT = -5.0f;
const int Terrain::_MAX_PIXEL_COLOR = 256 * 256 * 256;

Terrain::Terrain(int p_gridX, int p_gridZ, Loader * p_loader, TerrainTexturePack &p_texturePack, TerrainTexture &p_blendMap, const char * p_heightMapFilename)
{
	_texturePack = p_texturePack;
	_blendMap = p_blendMap;
	_x = (float)p_gridX * _SIZE;
	_z = (float)p_gridZ * _SIZE;
	_model = generateTerrain(p_loader, p_heightMapFilename);
}


Terrain::~Terrain()
{
}

float Terrain::getX() const
{
	return _x;
}

float Terrain::getZ() const
{
	return _z;
}

RawModel Terrain::getModel() const
{
	return _model;
}

TerrainTexturePack Terrain::getTexturePack() const
{
	return _texturePack;
}

TerrainTexture Terrain::getBlendMap() const
{
	return _blendMap;
}

float Terrain::getHeight(int p_x, int p_z, stb::image & p_img)
{
	if (p_x < 0 || p_x >= p_img.height() || p_z < 0 || p_z >= p_img.height())
	{
		return 0.0f;
	}
	double height = p_img.get_rgb(p_x, p_z);
	height /= _MAX_PIXEL_COLOR / 2;
	height -= 1.0;
	height *= _MAX_HEIGHT;
	return height;
}

float Terrain::getHeightOfTerrain(float p_worldX, float p_worldZ)
{
	float terrainX = p_worldX - _x;
	float terrainZ = p_worldZ - _z;
	float gridSquareSize = _SIZE / ((float)_heights.size() - 1);
	int gridX = (int)floor(terrainX / gridSquareSize);
	int gridZ = (int)floor(terrainZ / gridSquareSize);
	if (gridX >= _heights.size() - 1 || gridZ >= _heights.size() - 1 || gridX < 0 || gridZ < 0)
	{
		return 0.0f;
	}
	float xCoord = fmod(terrainX, gridSquareSize) / gridSquareSize;
	float zCoord = fmod(terrainZ, gridSquareSize) / gridSquareSize;
	float out;
	if (xCoord <= 1.0f - zCoord) {
		out = Maths::barryCentric(
			glm::fvec3(0.0f, _heights[gridX][gridZ], 0.0f), 
			glm::fvec3(1.0f, _heights[gridX + 1][gridZ], 0.0f), 
			glm::fvec3(0.0f, _heights[gridX][gridZ + 1], 1.0f), 
			glm::fvec2(xCoord, zCoord)
		);
	}
	else {
		out = Maths::barryCentric(
			glm::fvec3(1.0f, _heights[gridX + 1][gridZ], 0.0f), 
			glm::fvec3(1.0f, _heights[gridX + 1][gridZ + 1], 1.0f), 
			glm::fvec3(0.0f, _heights[gridX][gridZ + 1], 1.0f), 
			glm::fvec2(xCoord, zCoord)
		);
	}
	return out;
}

float Terrain::getSIZE()
{
	return _SIZE;
}

glm::fvec3 Terrain::calculateNormal(int p_x, int p_z, stb::image & p_heightmap, int p_vertexCount)
{
	int temp1 = p_x - 1 < 0 ? p_x : p_x - 1;
	int temp2 = p_x + 1 > p_vertexCount - 1 ? p_x : p_x + 1;
	int temp3 = p_z - 1 < 0 ? p_z : p_z - 1;
	int temp4 = p_z + 1 > p_vertexCount - 1 ? p_z : p_z + 1;

	float heightL = getHeight(temp1, p_z, p_heightmap);
	float heightR = getHeight(temp2, p_z, p_heightmap);
	float heightD = getHeight(p_x, temp3, p_heightmap);
	float heightU = getHeight(p_x, temp4, p_heightmap);
	glm::fvec3 normal(heightL - heightR, 2.0f, heightD - heightU);
	glm::normalize(normal);
	return normal;
}

RawModel Terrain::generateTerrain(Loader * p_loader, const char * p_heightMapFilename)
{
	stb::image image(p_heightMapFilename);

	const int _VERTEX_COUNT = image.height();
	_heights.resize(_VERTEX_COUNT, std::vector<float>(_VERTEX_COUNT));

	const int count = _VERTEX_COUNT * _VERTEX_COUNT;
	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<float> textureCoords;
	std::vector<unsigned int> indices;

	vertices.resize(count * 3);
	normals.resize(count * 3);
	textureCoords.resize(count * 2);
	indices.resize(6 * (_VERTEX_COUNT - 1)*(_VERTEX_COUNT - 1));

	int vertexPointer = 0;
	for (int i = 0; i < _VERTEX_COUNT; i++) 
	{
		for (int j = 0; j < _VERTEX_COUNT; j++)
		{
			vertices[vertexPointer * 3] = (float)j / ((float)_VERTEX_COUNT - 1.0f) * _SIZE;
			float height = getHeight(j, i, image);
			_heights[j][i] = height;
			vertices[vertexPointer * 3 + 1] = height;
			vertices[vertexPointer * 3 + 2] = (float)i / ((float)_VERTEX_COUNT - 1.0f) * _SIZE;
			glm::fvec3 normal = calculateNormal(j, i, image, _VERTEX_COUNT);
			normals[vertexPointer * 3] = normal.x;
			normals[vertexPointer * 3 + 1] = normal.y;
			normals[vertexPointer * 3 + 2] = normal.z;
			textureCoords[vertexPointer * 2] = (float)j / ((float)_VERTEX_COUNT - 1.0f);
			textureCoords[vertexPointer * 2 + 1] = (float)i / ((float)_VERTEX_COUNT - 1.0f);
			vertexPointer++;
		}
	}
	int pointer = 0;
	for (int gz = 0; gz < _VERTEX_COUNT - 1; gz++) 
	{
		for (int gx = 0; gx < _VERTEX_COUNT - 1; gx++) 
		{
			int topLeft = (gz * _VERTEX_COUNT) + gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz + 1) * _VERTEX_COUNT) + gx;
			int bottomRight = bottomLeft + 1;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topRight;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
		}
	}
	return p_loader->loadToVAO(vertices, textureCoords, normals, indices);
}
