#include "RawModel.h"
#include "ModelTexture.h"
#include "Loader.h"
#include "TerrainTexturePack.h"
#include "Maths.h"

#ifndef TERRAIN_H
#define TERRAIN_H

class Terrain
{
public:
	explicit Terrain(int p_gridX, int p_gridZ, Loader * p_loader, TerrainTexturePack &p_texturePack, TerrainTexture &p_blendMap, const char * p_heightMapFilename);
	~Terrain();

	static float getSIZE();

	float getX() const;
	float getZ() const;
	RawModel getModel() const;
	TerrainTexturePack getTexturePack() const;
	TerrainTexture getBlendMap() const;
	float getHeightOfTerrain(float p_worldX, float p_worldZ);

private:
	static const float _SIZE;
	static const float _MAX_HEIGHT;
	static const float _MIN_HEIGHT;
	static const int _MAX_PIXEL_COLOR;

	float _x;
	float _z;
	RawModel _model;
	TerrainTexturePack _texturePack;
	TerrainTexture _blendMap;
	std::vector<std::vector<float>> _heights;

	float getHeight(int p_x, int p_z, stb::image & p_img);
	glm::fvec3 calculateNormal(int p_x, int p_z, stb::image & p_heightmap, int p_vertexCount);
	RawModel generateTerrain(Loader * p_loader, const char * p_heightMapFilename);
};


#endif TERRAIN_H