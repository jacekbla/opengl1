#include "Culler.h"



Culler::Culler(float p_fov, float p_aspectRatio, float p_nearPlaneDist, float p_farPlaneDist, Camera * p_camera)
{
	float yaw = p_camera->getYaw() - 90.0f;
	float pitch = p_camera->getPitch();
	glm::vec3 look(
		cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
		-sin(glm::radians(pitch)),
		sin(glm::radians(yaw)) * cos(glm::radians(pitch)));

	_cameraLookDirection = glm::normalize(look);

	pitch -= 90.0f;
	glm::vec3 look2(
		cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
		-sin(glm::radians(pitch)),
		sin(glm::radians(yaw)) * cos(glm::radians(pitch)));
	_cameraUpDirection = glm::normalize(look2);

	_cameraRightDirection = glm::cross(_cameraLookDirection, _cameraUpDirection);
	glm::normalize(_cameraRightDirection);

	_cameraPos = p_camera->getPosition();
	_farPlaneCenter = _cameraPos + _cameraLookDirection * p_farPlaneDist;
	_nearPlaneCenter = _cameraPos + _cameraLookDirection * p_nearPlaneDist;

	_farPlaneDist = p_farPlaneDist;
	_nearPlaneDist = p_nearPlaneDist;
	_fov = p_fov;

	nearWidth = 2.0f * _nearPlaneDist * tan(glm::radians(p_fov / 2.0f));
	nearHeight = nearWidth / p_aspectRatio;
	farWidth = 2.0f * _farPlaneDist * tan(glm::radians(p_fov / 2.0f));
	farHeight = farWidth / p_aspectRatio;

	extractPlanes();
}

Culler::~Culler()
{
}

void Culler::updateCuller(Camera * p_camera)
{
	float yaw = p_camera->getYaw() - 90.0f;
	float pitch = p_camera->getPitch();
	glm::vec3 look(
		cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
		-sin(glm::radians(pitch)),
		sin(glm::radians(yaw)) * cos(glm::radians(pitch)));

	_cameraLookDirection = glm::normalize(look);

	pitch -= 90.0f;
	glm::vec3 look2(
		cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
		-sin(glm::radians(pitch)),
		sin(glm::radians(yaw)) * cos(glm::radians(pitch)));
	_cameraUpDirection = glm::normalize(look2);

	_cameraRightDirection = glm::cross(_cameraLookDirection, _cameraUpDirection);
	_cameraRightDirection = glm::normalize(_cameraRightDirection);

	_cameraPos = p_camera->getPosition();
	_farPlaneCenter = _cameraPos + _cameraLookDirection * _farPlaneDist;
	_nearPlaneCenter = _cameraPos + _cameraLookDirection * _nearPlaneDist;

	extractPlanes();
}

bool Culler::frustrumCulling(std::vector<Entity> & p_entities)
{
	std::vector<Entity> inside;
	for (Entity e : p_entities)
	{
		std::pair<glm::vec3, float> sphere = e.getModel().getRawModel().getBoundingSphere();
		sphere.first += e.getPosition();
		sphere.second *= e.getScale();
		bool outside = false;
		for (int i = 0; i < 6; i++)
		{
			float distance = glm::dot(_planes[i].second, sphere.first - _planes[i].first);
			if (distance < -sphere.second)
			{//outside
				outside = true;
				break;
			}
		}
		if (!outside)
		{
			inside.push_back(e);
		}
	}
	p_entities = inside;
	return true;
}

void Culler::extractPlanes()
{
	//right
	glm::vec3 temp = (_nearPlaneCenter + _cameraRightDirection * nearWidth / 2.0f) - _cameraPos;
	temp = glm::normalize(temp);
	glm::vec3 right_n = glm::cross(_cameraUpDirection, temp);
	_planes[RIGHT] = std::pair<glm::vec3, glm::vec3>(_cameraPos, right_n);

	//left
	temp = (_nearPlaneCenter - _cameraRightDirection * nearWidth / 2.0f) - _cameraPos;
	temp = glm::normalize(temp);
	glm::vec3 left_n = glm::cross(temp, _cameraUpDirection);
	_planes[LEFT] = std::pair<glm::vec3, glm::vec3>(_cameraPos, left_n);

	//top
	temp = (_nearPlaneCenter + _cameraUpDirection * nearHeight / 2.0f) - _cameraPos;
	temp = glm::normalize(temp);
	glm::vec3 top_n = glm::cross(temp, _cameraRightDirection);
	_planes[TOP] = std::pair<glm::vec3, glm::vec3>(_cameraPos, top_n);

	//bottom
	temp = (_nearPlaneCenter - _cameraUpDirection * nearHeight / 2.0f) - _cameraPos;
	temp = glm::normalize(temp);
	glm::vec3 bottom_n = glm::cross(_cameraRightDirection, temp);
	_planes[BOTTOM] = std::pair<glm::vec3, glm::vec3>(_cameraPos, bottom_n);

	//near
	_planes[NEAR] = std::pair<glm::vec3, glm::vec3>(_nearPlaneCenter, _cameraLookDirection);
	
	//far
	_planes[FAR] = std::pair<glm::vec3, glm::vec3>(_farPlaneCenter, -_cameraLookDirection);
}
