#include "TerrainShader.h"
#include "Terrain.h"
#include "ModelTexture.h"
#include "TerrainTexturePack.h";

#ifndef TERRAIN_RENDERER_H
#define TERRAIN_RENDERER_H

class TerrainRenderer
{
public:
	TerrainRenderer(TerrainShader* p_shader, glm::mat4 p_projectionMatrix) : _shader(p_shader)
	{
		p_shader->start();
		p_shader->loadProjectionMatrix(p_projectionMatrix);
		p_shader->connectTextureUnits();
		p_shader->stop();
	};
	~TerrainRenderer();

	void render(std::vector<Terrain> p_terrains);
private:
	TerrainShader *_shader;

	void prepareTerrain(Terrain & p_terrain);
	void unbindTexturedModel();
	void loadModelMatrix(Terrain & p_terrain);
	void bindTextures(Terrain & p_terrain);
};


#endif TERRAIN_RENDERER_H