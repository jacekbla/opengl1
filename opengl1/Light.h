#include "Utility.h"

#ifndef LIGHT_H
#define LIGHT_H


class Light
{
public:
	Light(glm::fvec3 p_position, glm::fvec3 p_color) :
		_position(p_position),
		_color(p_color),
		_attenuation(glm::fvec3(1.0f, 0.0f, 0.0f)) {};
	Light(glm::fvec3 p_position, glm::fvec3 p_color, glm::fvec3 p_attenuation) :
		_position(p_position),
		_color(p_color),
		_attenuation(p_attenuation) {};
	~Light();

	glm::fvec3 &getPostion();
	glm::fvec3 &getColor();
	glm::fvec3 &getAttenuation();
	void setPostion(glm::fvec3 p_position);
	void setColor(glm::fvec3 p_color);

private:
	glm::fvec3 _position;
	glm::fvec3 _color;
	glm::fvec3 _attenuation;
};


#endif LIGHT_H