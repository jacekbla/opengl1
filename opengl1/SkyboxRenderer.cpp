#include "SkyboxRenderer.h"

const float SkyboxRenderer::_SIZE = 500.0f;
const float SkyboxRenderer::_SPEED = 0.0001f;
const std::vector<const char *> SkyboxRenderer::_TEXTURES_FILES = {
	"res/skybox/skybox_texture_right3.png", 
	"res/skybox/skybox_texture_left3.png", 
	"res/skybox/skybox_texture_top3.png", 
	"res/skybox/skybox_texture_bottom3.png", 
	"res/skybox/skybox_texture_back3.png", 
	"res/skybox/skybox_texture_front3.png"
};
const std::vector<float> SkyboxRenderer::_VERTICES = {
		-_SIZE,  _SIZE, -_SIZE,
		-_SIZE, -_SIZE, -_SIZE,
		_SIZE, -_SIZE, -_SIZE,
		 _SIZE, -_SIZE, -_SIZE,
		 _SIZE,  _SIZE, -_SIZE,
		-_SIZE,  _SIZE, -_SIZE,

		-_SIZE, -_SIZE,  _SIZE,
		-_SIZE, -_SIZE, -_SIZE,
		-_SIZE,  _SIZE, -_SIZE,
		-_SIZE,  _SIZE, -_SIZE,
		-_SIZE,  _SIZE,  _SIZE,
		-_SIZE, -_SIZE,  _SIZE,

		 _SIZE, -_SIZE, -_SIZE,
		 _SIZE, -_SIZE,  _SIZE,
		 _SIZE,  _SIZE,  _SIZE,
		 _SIZE,  _SIZE,  _SIZE,
		 _SIZE,  _SIZE, -_SIZE,
		 _SIZE, -_SIZE, -_SIZE,

		-_SIZE, -_SIZE,  _SIZE,
		-_SIZE,  _SIZE,  _SIZE,
		 _SIZE,  _SIZE,  _SIZE,
		 _SIZE,  _SIZE,  _SIZE,
		 _SIZE, -_SIZE,  _SIZE,
		-_SIZE, -_SIZE,  _SIZE,

		-_SIZE,  _SIZE, -_SIZE,
		 _SIZE,  _SIZE, -_SIZE,
		 _SIZE,  _SIZE,  _SIZE,
		 _SIZE,  _SIZE,  _SIZE,
		-_SIZE,  _SIZE,  _SIZE,
		-_SIZE,  _SIZE, -_SIZE,

		-_SIZE, -_SIZE, -_SIZE,
		-_SIZE, -_SIZE,  _SIZE,
		 _SIZE, -_SIZE, -_SIZE,
		 _SIZE, -_SIZE, -_SIZE,
		-_SIZE, -_SIZE,  _SIZE,
		 _SIZE, -_SIZE,  _SIZE
};


SkyboxRenderer::SkyboxRenderer(Loader * p_loader, glm::fmat4 p_projMatrix)
{
	_cube = new RawModel(p_loader->loadToVAO(_VERTICES, 3));
	_texture = p_loader->loadCubeMapNoAlpha(_TEXTURES_FILES);
	_shader = new SkyboxShader();
	_shader->start();
	_shader->loadProjectionMatrix(p_projMatrix);
	_shader->stop();

}


SkyboxRenderer::~SkyboxRenderer()
{
	delete _cube;
	delete _shader;
}

SkyboxRenderer & SkyboxRenderer::operator=(const SkyboxRenderer &p_other)
{
	_cube = p_other.getCube();
	_texture = p_other.getTexture();
	_shader = p_other.getShader();
	return(*this);
}

RawModel * SkyboxRenderer::getCube() const
{
	return _cube;
}

int SkyboxRenderer::getTexture() const
{
	return _texture;
}

SkyboxShader * SkyboxRenderer::getShader() const
{
	return _shader;
}

void SkyboxRenderer::render(Camera & p_camera)
{
	_shader->start();
	_shader->loadViewMatrix(p_camera);
	glBindVertexArray(_cube->getVaoID());
	glEnableVertexAttribArray(0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _texture);

	_rot += _SPEED * DisplayManager::getFrameTimeMiliseconds();
	glm::mat4 transformMatrix = Maths::createTransformMatrix(glm::vec3(0.0f, 0.0f, 0.0f), 0.0f, _rot, 0.0f, 1.0f);
	_shader->loadTransformMatrix(transformMatrix);

	glDrawArrays(GL_TRIANGLES, 0, _cube->getVertexCount());
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
	_shader->stop();
}
