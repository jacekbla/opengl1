#include "Utility.h"
#include "StaticShader.h"
#include "EntityRenderer.h"
#include "TexturedModel.h"
#include "Entity.h"
#include "Light.h"
#include "Camera.h"
#include "TerrainRenderer.h"
#include "SkyboxRenderer.h"
#include <map>

#ifndef MASTER_RENDERER_H
#define MASTER_RENDERER_H

class MasterRenderer
{
public:
	explicit MasterRenderer();
	explicit MasterRenderer(Loader * p_loader);
	~MasterRenderer();

	void prepare();
	void cleanUp();
	void render(std::vector<Light> & p_lights, Camera & p_camera, glm::fvec4 & p_clipPlane);
	void processEntity(Entity p_entity);
	void processTerrain(Terrain p_terrain);
	glm::mat4 getProjectionMatrix();
	static glm::fvec3 getSKY_COLOR();
	static int getLIGHT_COUNT();
	static float getFOV();
	static float getNEAR_PLANE();
	static float getFAR_PLANE();

private:
	static const float _FOV;
	static const float _NEAR_PLANE;
	static const float _FAR_PLANE;
	static const int _LIGHT_COUNT = 4;

	static const glm::fvec3 _SKY_COLOR;

	glm::mat4 _projectionMatrix;

	StaticShader *_shader;
	EntityRenderer *_renderer;

	TerrainShader *_terrainShader;
	TerrainRenderer *_terrainRenderer;

	SkyboxRenderer * _skyboxRenderer;

	void createProjectionMatrix();
	std::map <int, std::pair<TexturedModel, std::vector<Entity>> > _entities;
	std::vector<Terrain> _terrains;
};


#endif MASTER_RENDERER_H