#include "WaterRenderer.h"


const float WaterRenderer::_WAVE_SPEED = 0.0001f;
const float WaterRenderer::_HEIGHT = 0.0f;

WaterRenderer::WaterRenderer(glm::mat4 p_projMatrix, WaterFrameBuffers p_fbos)
{
	_fbos = p_fbos;
	_shader.start();
	_shader.connectTextureUnits();
	_shader.loadProjectionMatrix(p_projMatrix);
	_shader.stop();
}

WaterRenderer::~WaterRenderer()
{
	_shader.cleanUp();
}

float WaterRenderer::getHEIGHT()
{
	return _HEIGHT;
}

void WaterRenderer::render(Camera & p_camera, std::vector<Light> & p_lights, std::vector<WaterTile> quads)
{
	for (WaterTile quad : quads)
	{
		prepare(p_camera, p_lights, quad);

		glm::mat4 modelMatrix = Maths::createTransformMatrix(glm::fvec3(quad.getX(), quad.getHeight(), quad.getZ()), 0.0f, 0.0f, 0.0f, WaterTile::TILE_SIZE);
		_shader.loadModelMatrix(modelMatrix);

		glDrawArrays(GL_TRIANGLES, 0, quad.getVertexCount());

		unbind();
	}
}

void WaterRenderer::prepare(Camera & p_camera, std::vector<Light> &p_lights, WaterTile & p_waterTile)
{
	_shader.start();
	updateTime();
	_shader.loadViewMatrix(p_camera);
	_shader.loadHeight(_HEIGHT);
	_shader.loadSkyColor(MasterRenderer::getSKY_COLOR().r, MasterRenderer::getSKY_COLOR().g, MasterRenderer::getSKY_COLOR().b);
	_shader.loadLights(p_lights);
	glBindVertexArray(p_waterTile.getVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _fbos.getReflectionTexture());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _fbos.getRefractionTexture());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _fbos.getRefractionDepthTexture());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void WaterRenderer::unbind()
{
	glDisable(GL_BLEND);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);
	_shader.stop();
}

void WaterRenderer::updateTime()
{
	_waveTime += _WAVE_SPEED * DisplayManager::getFrameTimeMiliseconds();
	_shader.loadWaveTime(_waveTime);
}
