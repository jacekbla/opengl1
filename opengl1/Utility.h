#ifndef UTILITY_H
#define UTILITY_H

#include <GL/glew.h>
#include <glfw/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <istream>
#include <map>


#endif UTILITY_H