#include "Utility.h"
#include "WaterShader.h"
#include "Loader.h"
#include "RawModel.h"
#include "WaterTile.h"
#include "Maths.h"
#include "WaterFrameBuffers.h"
#include "DisplayManager.h"
#include "Light.h"
#include "MasterRenderer.h"

#ifndef WATER_RENDERER_H
#define WATER_RENDERER_H


class WaterRenderer
{
public:
	explicit WaterRenderer(glm::mat4 p_projMatrix, WaterFrameBuffers p_fbos);
	~WaterRenderer();

	static float getHEIGHT();

	void render(Camera &p_camera, std::vector<Light> &p_lights, std::vector<WaterTile> quads);

private:
	static const float _WAVE_SPEED;
	static const float _HEIGHT;

	WaterShader _shader;
	WaterFrameBuffers _fbos;
	float _waveTime = 0.0f;

	void prepare(Camera &p_camera, std::vector<Light> &p_lights, WaterTile & p_waterTile);
	void unbind();
	void updateTime(); 
	//void setUpVAO(Loader p_loader);
};


#endif WATER_RENDERER_H