#include "Camera.h"

Camera::~Camera()
{
}

glm::vec3 Camera::getPosition() const
{
	return _position;
}

float Camera::getPitch() const
{
	return _pitch;
}

float Camera::getYaw() const
{
	return _yaw;
}

float Camera::getRoll() const
{
	return _roll;
}

void Camera::setPosition(glm::vec3 p_position)
{
	_position = p_position;
}

void Camera::invertPitch()
{
	_pitch = -_pitch;
}

void Camera::move(GLFWwindow* p_window)
{
	if (glfwGetKey(p_window, GLFW_KEY_W))
	{
		float xStep = _MOVE_SENSITIVITY * sin(glm::radians(_yaw));
		float zStep = _MOVE_SENSITIVITY * cos(glm::radians(_yaw));

		_position.x += xStep * DisplayManager::getFrameTimeMiliseconds();
		_position.z -= zStep * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_D))
	{
		float xStep = _MOVE_SENSITIVITY * cos(glm::radians(_yaw));
		float zStep = _MOVE_SENSITIVITY * sin(glm::radians(_yaw));

		_position.x += xStep * DisplayManager::getFrameTimeMiliseconds();
		_position.z += zStep * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_A))
	{
		float xStep = _MOVE_SENSITIVITY * cos(glm::radians(_yaw));
		float zStep = _MOVE_SENSITIVITY * sin(glm::radians(_yaw));

		_position.x -= xStep * DisplayManager::getFrameTimeMiliseconds();
		_position.z -= zStep * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_S))
	{
		float xStep = _MOVE_SENSITIVITY * sin(glm::radians(_yaw));
		float zStep = _MOVE_SENSITIVITY * cos(glm::radians(_yaw));

		_position.x -= xStep * DisplayManager::getFrameTimeMiliseconds();
		_position.z += zStep * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_SPACE))
	{
		_position.y += _MOVE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_C))
	{
		_position.y -= _MOVE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_DOWN))
	{
		_pitch += _KEYBOARD_ANGLE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_UP))
	{
		_pitch -= _KEYBOARD_ANGLE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_LEFT))
	{
		_yaw -= _KEYBOARD_ANGLE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
	}
	if (glfwGetKey(p_window, GLFW_KEY_RIGHT))
	{
		_yaw += _KEYBOARD_ANGLE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
	}

	glfwSetInputMode(p_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	double* mousePosX = new double;
	double* mousePosY = new double;
	glfwGetCursorPos(p_window, mousePosX, mousePosY);
	if ((glfwGetMouseButton(p_window, GLFW_MOUSE_BUTTON_2)))
	{
		if ((glfwGetMouseButton(p_window, GLFW_MOUSE_BUTTON_1)))
		{
			float xStep = _MOVE_SENSITIVITY * sin(glm::radians(_yaw));
			float zStep = _MOVE_SENSITIVITY * cos(glm::radians(_yaw));

			_position.x += xStep * 6.0f * DisplayManager::getFrameTimeMiliseconds();
			_position.z -= zStep * 6.0f * DisplayManager::getFrameTimeMiliseconds();
		}

		double diffX = *mousePosX - _currMouseX;
		double diffY = *mousePosY - _currMouseY;
		if (diffX != 0 || diffY != 0)
		{
			_yaw += (float) diffX * _MOUSE_ANGLE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
			_pitch += (float) diffY * _MOUSE_ANGLE_SENSITIVITY * DisplayManager::getFrameTimeMiliseconds();
		}
	}
	_currMouseX = *mousePosX;
	_currMouseY = *mousePosY;
	delete mousePosX;
	delete mousePosY;
}

void Camera::moveForward()
{
	float xStep = _MOVE_SENSITIVITY * 3.0f * sin(glm::radians(_yaw));
	float zStep = _MOVE_SENSITIVITY * 3.0f * cos(glm::radians(_yaw));

	_position.x += xStep;
	_position.z -= zStep;
}

void Camera::moveUp()
{
	float yStep = _MOVE_SENSITIVITY * 3.0f;

	_position.y += yStep;
}

void Camera::turnLeft()
{
	_yaw -= _KEYBOARD_ANGLE_SENSITIVITY * 30.0f;
}

void Camera::turnRight()
{
	_yaw += _KEYBOARD_ANGLE_SENSITIVITY * 30.0f;
}

void Camera::turnUp()
{
	_pitch -= _KEYBOARD_ANGLE_SENSITIVITY * 30.0f;
}

void Camera::turnDown()
{
	_pitch += _KEYBOARD_ANGLE_SENSITIVITY * 30.0f;
}
