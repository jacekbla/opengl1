#include "OBJLoader.h"


bool loadOBJ(
	const char * p_path,
	std::vector<glm::vec3> & out_vertices,
	std::vector<glm::vec2> & out_uvs,
	std::vector<glm::vec3> & out_normals,
	std::vector<unsigned int> & out_indices) 
{
	printf("Loading OBJ file %s...\n", p_path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE * file = fopen(p_path, "r");
	if (file == NULL) {
		printf("Impossible to open the file !\n");
		return false;
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);
		out_indices.push_back(i);

	}

	return true;
}

bool comp(std::pair<float, std::pair<unsigned int, unsigned int>> p_ele1, std::pair<float, std::pair<unsigned int, unsigned int>> p_ele2)
{
	return p_ele1.first < p_ele2.first;
}

void removeAndMerge(std::vector<unsigned int> & p_indices, int survivorID, int deletedID)
{
	std::vector<int> idsToRemove;

	for (int i = 0; i < p_indices.size(); i += 3)
	{
		int vertID1 = p_indices[i];
		int vertID2 = p_indices[i + 1];
		int vertID3 = p_indices[i + 2];

		if (vertID1 == deletedID)
		{
			if (vertID2 == survivorID || vertID3 == survivorID)
			{
				idsToRemove.push_back(i);
			}
			else
			{
				p_indices[i] = survivorID;
			}
		}

		if (vertID2 == deletedID)
		{
			if (vertID1 == survivorID || vertID3 == survivorID)
			{
				idsToRemove.push_back(i);
			}
			else
			{
				p_indices[i + 1] = survivorID;
			}
		}

		if (vertID3 == deletedID)
		{
			if (vertID1 == survivorID || vertID2 == survivorID)
			{
				idsToRemove.push_back(i);
			}
			else
			{
				p_indices[i + 2] = survivorID;
			}
		}
	}

	for (int i = 0; i < idsToRemove.size(); i++)
	{
		if (idsToRemove[i] + 3 - 3 * i == p_indices.size())
		{
			p_indices.pop_back();
			p_indices.pop_back();
			p_indices.pop_back();
		}
		else
		{
			p_indices.erase(p_indices.begin() + idsToRemove[i] - 3 * i, p_indices.begin() + idsToRemove[i] + 3 - 3 * i);
		}
	}
}

bool simplifyMesh1(std::vector<unsigned int> & p_indices, std::vector<glm::vec3> & p_vertices)
{
	//calc shortest edges
	std::vector<std::pair<unsigned int, unsigned int>> shortestEdges;

	std::vector<std::pair<float, std::pair<unsigned int, unsigned int>>> shortestEdgesDist;

	float shortestDist = std::numeric_limits<float>::max();
	for (int i = 0; i < p_indices.size(); i += 3)
	{
		int vertID1 = p_indices[i];
		int vertID2 = p_indices[i + 1];
		int vertID3 = p_indices[i + 2];

		float dist1 = glm::distance(p_vertices[vertID1], p_vertices[vertID2]);
		float dist2 = glm::distance(p_vertices[vertID1], p_vertices[vertID3]);
		float dist3 = glm::distance(p_vertices[vertID3], p_vertices[vertID2]);

		std::pair<float, std::pair<unsigned int, unsigned int>> edge1(dist1, std::pair<unsigned int, unsigned int>(vertID1, vertID2));
		std::pair<float, std::pair<unsigned int, unsigned int>> edge1_inv(dist1, std::pair<unsigned int, unsigned int>(vertID2, vertID1));
		std::pair<float, std::pair<unsigned int, unsigned int>> edge2(dist2, std::pair<unsigned int, unsigned int>(vertID1, vertID3));
		std::pair<float, std::pair<unsigned int, unsigned int>> edge2_inv(dist2, std::pair<unsigned int, unsigned int>(vertID3, vertID1));
		std::pair<float, std::pair<unsigned int, unsigned int>> edge3(dist3, std::pair<unsigned int, unsigned int>(vertID3, vertID2));
		std::pair<float, std::pair<unsigned int, unsigned int>> edge3_inv(dist3, std::pair<unsigned int, unsigned int>(vertID2, vertID3));


		if (dist1 > 0.0f)
		{
			if (shortestEdgesDist.end() == std::find(shortestEdgesDist.begin(), shortestEdgesDist.end(), edge1) &&
				shortestEdgesDist.end() == std::find(shortestEdgesDist.begin(), shortestEdgesDist.end(), edge1_inv))
			{
				shortestEdgesDist.push_back(edge1);
			}
		}

		if (dist2 > 0.0f)
		{
			if (shortestEdgesDist.end() == std::find(shortestEdgesDist.begin(), shortestEdgesDist.end(), edge2) &&
				shortestEdgesDist.end() == std::find(shortestEdgesDist.begin(), shortestEdgesDist.end(), edge2_inv))
			{
				shortestEdgesDist.push_back(edge2);
			}
		}

		if (dist3 > 0.0f)
		{
			if (shortestEdgesDist.end() == std::find(shortestEdgesDist.begin(), shortestEdgesDist.end(), edge3) &&
				shortestEdgesDist.end() == std::find(shortestEdgesDist.begin(), shortestEdgesDist.end(), edge3_inv))
			{
				shortestEdgesDist.push_back(edge3);
			}
		}
	}

	std::sort(shortestEdgesDist.begin(), shortestEdgesDist.end(), comp);
	for (std::pair<float, std::pair<unsigned int, unsigned int>> edge : shortestEdgesDist)
	{
		shortestEdges.push_back(edge.second);
	}


	//check if shortest is ok, if not pick next, if no next return false
	int deletedID;
	int survivorID;

	bool found = false;
	for (std::pair<unsigned int, unsigned int> shortestEdge : shortestEdges)
	{
		deletedID = shortestEdge.first;
		survivorID = shortestEdge.second;

		std::vector<int> trianglesToMergeIDs;
		//check for triangles containing the shortest edge, there must be 2
		int counter = 0;
		for (int i = 0; i < p_indices.size(); i += 3)
		{
			int vertID1 = p_indices[i];
			int vertID2 = p_indices[i + 1];
			int vertID3 = p_indices[i + 2];

			if ((vertID1 == deletedID && vertID2 == survivorID) ||
				(vertID1 == deletedID && vertID3 == survivorID) ||
				(vertID3 == deletedID && vertID2 == survivorID) ||
				(vertID1 == survivorID && vertID2 == deletedID) ||
				(vertID1 == survivorID && vertID3 == deletedID) ||
				(vertID3 == survivorID && vertID2 == deletedID))
			{
				counter++;
			}
			else if (vertID1 == deletedID || vertID2 == deletedID || vertID3 == deletedID)
			{
				trianglesToMergeIDs.push_back(i);
			}
		}
		if (counter != 2)
		{
			continue;
		}

		//check for triangle flips
		//check
		//get normals pre merge
		std::vector<glm::vec3> normalsPreMerge;
		for (int i : trianglesToMergeIDs)
		{
			int vertID1 = p_indices[i];
			int vertID2 = p_indices[i + 1];
			int vertID3 = p_indices[i + 2];

			glm::vec3 normal = glm::cross(p_vertices[vertID2] - p_vertices[vertID1], p_vertices[vertID3] - p_vertices[vertID1]);
			normalsPreMerge.push_back(normal);
		}

		//get normals post merge
		std::vector<glm::vec3> normalsPostMerge;
		for (int i : trianglesToMergeIDs)
		{
			int vertID1 = p_indices[i];
			int vertID2 = p_indices[i + 1];
			int vertID3 = p_indices[i + 2];
			if (vertID1 == deletedID)
			{
				vertID1 = survivorID;
			}
			if (vertID2 == deletedID)
			{
				vertID2 = survivorID;
			}
			if (vertID3 == deletedID)
			{
				vertID3 = survivorID;
			}

			glm::vec3 normal = glm::cross(p_vertices[vertID2] - p_vertices[vertID1], p_vertices[vertID3] - p_vertices[vertID1]);
			normalsPostMerge.push_back(normal);
		}
		//compare normals
		bool flipped = false;
		for(int i = 0; i < normalsPreMerge.size(); i++)
		{
			glm::vec3 normal1 = glm::normalize(normalsPreMerge[i]);
			glm::vec3 normal2 = glm::normalize(normalsPostMerge[i]);
			float dot = glm::dot(normal1, normal2);
			if (dot < 0.0f)
			{
				flipped = true;
				break;
			}
		}

		//if flipped, check with survivor as deleted
		if (flipped)
		{
			flipped = false;
			trianglesToMergeIDs.clear();
			deletedID = shortestEdge.second;
			survivorID = shortestEdge.first;
			for (int i = 0; i < p_indices.size(); i += 3)
			{
				int vertID1 = p_indices[i];
				int vertID2 = p_indices[i + 1];
				int vertID3 = p_indices[i + 2];
				if (vertID1 == deletedID || vertID2 == deletedID || vertID3 == deletedID)
				{
					trianglesToMergeIDs.push_back(i);
				}
			}

			//check again
			//get normals pre merge
			normalsPreMerge.clear();
			for (int i : trianglesToMergeIDs)
			{
				int vertID1 = p_indices[i];
				int vertID2 = p_indices[i + 1];
				int vertID3 = p_indices[i + 2];

				glm::vec3 normal = glm::cross(p_vertices[vertID2] - p_vertices[vertID1], p_vertices[vertID3] - p_vertices[vertID1]);
				normalsPreMerge.push_back(normal);
			}

			//get normals post merge
			normalsPostMerge.clear();
			for (int i : trianglesToMergeIDs)
			{
				int vertID1 = p_indices[i];
				int vertID2 = p_indices[i + 1];
				int vertID3 = p_indices[i + 2];
				if (vertID1 == deletedID)
				{
					vertID1 = survivorID;
				}
				if (vertID2 == deletedID)
				{
					vertID2 = survivorID;
				}
				if (vertID3 == deletedID)
				{
					vertID3 = survivorID;
				}

				glm::vec3 normal = glm::cross(p_vertices[vertID2] - p_vertices[vertID1], p_vertices[vertID3] - p_vertices[vertID1]);
				normalsPostMerge.push_back(normal);
			}
			//compare normals
			for (int i = 0; i < normalsPreMerge.size(); i++)
			{
				glm::vec3 normal1 = glm::normalize(normalsPreMerge[i]);
				glm::vec3 normal2 = glm::normalize(normalsPostMerge[i]);
				float dot = glm::dot(normal1, normal2);
				if (dot < 0.0f)
				{
					flipped = true;
					break;
				}
			}
			if (flipped)
			{
				continue;
			}
		}

		found = true;
		break;
	}
	if (!found)
	{
		return false;
	}


	//remove and merge triangles
	removeAndMerge(p_indices, survivorID, deletedID);

	return true;
}

RawModel loadOBJ(const char* p_path, Loader * p_loader)
{
	printf("Loading OBJ file %s...\n", p_path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;

	std::vector<glm::vec3> out_vertices;
	std::vector<glm::vec2> out_uvs;
	std::vector<glm::vec3> out_normals;
	std::vector<unsigned int> out_indices;


	FILE * file = fopen(p_path, "r");
	if (file == NULL) {
		printf("Impossible to open the file !\n");
		return RawModel();
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				return RawModel();
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);
		out_indices.push_back(i);

	}

	//sphere
	glm::vec3 sum(0.0f, 0.0f, 0.0f);
	for (glm::vec3 v : out_vertices)
	{
		sum += v;
	}
	glm::vec3 avg(sum.x / out_vertices.size(), sum.y / out_vertices.size(), sum.z / out_vertices.size());
	float radius = 0.0f;
	for (glm::vec3 v : out_vertices)
	{
		float dist = glm::distance(avg, v);
		if (dist > radius)
		{
			radius = dist;
		}
	}

	std::vector<float> points;
	std::vector<float> texCoords;
	std::vector<float> normals;
	std::vector<unsigned int> indices;
	for (int i = 0; i < out_vertices.size(); ++i)
	{
		points.push_back(out_vertices[i].x);
		points.push_back(out_vertices[i].y);
		points.push_back(out_vertices[i].z);

		texCoords.push_back(out_uvs[i].x);
		texCoords.push_back(out_uvs[i].y);

		normals.push_back(out_normals[i].x);
		normals.push_back(out_normals[i].y);
		normals.push_back(out_normals[i].z);

		indices.push_back(out_indices[i]);
	}

	return p_loader->loadToVAO_sphere(points, texCoords, normals, indices, std::pair<glm::vec3, float>(avg, radius));
}

std::vector<RawModel> loadOBJ_lod(const char* p_path, Loader * p_loader)
{
	printf("Loading OBJ file %s...\n", p_path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;

	std::vector<glm::vec3> out_vertices;
	std::vector<glm::vec2> out_uvs;
	std::vector<glm::vec3> out_normals;
	std::vector<unsigned int> out_indices;


	FILE * file = fopen(p_path, "r");
	if (file == NULL) {
		printf("Impossible to open the file !\n");
		return std::vector<RawModel>();
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				return std::vector<RawModel>();
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);
		out_indices.push_back(i);

	}


	std::vector<float> points;
	std::vector<float> texCoords;
	std::vector<float> normals;
	std::vector<unsigned int> indices;
	for (int i = 0; i < out_vertices.size(); ++i)
	{
		points.push_back(out_vertices[i].x);
		points.push_back(out_vertices[i].y);
		points.push_back(out_vertices[i].z);

		texCoords.push_back(out_uvs[i].x);
		texCoords.push_back(out_uvs[i].y);

		normals.push_back(out_normals[i].x);
		normals.push_back(out_normals[i].y);
		normals.push_back(out_normals[i].z);

		indices.push_back(out_indices[i]);
	}

	std::vector<RawModel> models;
	//lod0
	models.push_back(p_loader->loadToVAO(points, texCoords, normals, indices));

	for (int i = 0; i < out_indices.size(); ++i)
	{
		glm::vec3 pos = out_vertices[i];
		for (int j = i; j < out_indices.size(); ++j)
		{
			if (out_vertices[j] == pos)
			{
				out_indices[j] = out_indices[i];
			}
		}
	}


	//lod1
	for (int i = 0; i < out_vertices.size() / 2; i++)
	{
		bool res = simplifyMesh1(out_indices, out_vertices);
		std::cout << res << "1_";
		std::cout << i << std::endl;
	}
	models.push_back(p_loader->loadToVAO(points, texCoords, normals, out_indices));

	//lod2
	for (int i = 0; i < out_vertices.size() / 2; i++)
	{
		bool res = simplifyMesh1(out_indices, out_vertices);
		std::cout << res << "2_";
		std::cout << i << std::endl;
	}
	models.push_back(p_loader->loadToVAO(points, texCoords, normals, out_indices));

	//lod3
	for (int i = 0; i < out_vertices.size() / 2; i++)
	{
		bool res = simplifyMesh1(out_indices, out_vertices);
		std::cout << res << "3_";
		std::cout << i << std::endl;
	}
	models.push_back(p_loader->loadToVAO(points, texCoords, normals, out_indices));

	return models;
}