#include "Light.h"



Light::~Light()
{
}

glm::fvec3 &Light::getPostion()
{
	return _position;
}

glm::fvec3 &Light::getColor()
{
	return _color;
}

glm::fvec3 & Light::getAttenuation()
{
	return _attenuation;
}

void Light::setPostion(glm::fvec3 p_position)
{
	_position = p_position;
}

void Light::setColor(glm::fvec3 p_color)
{
	_color = p_color;
}
