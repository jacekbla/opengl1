#include "DisplayManager.h"

const char* DisplayManager::_TITLE = "openGL";

std::chrono::time_point<std::chrono::system_clock> DisplayManager::_start = std::chrono::system_clock::now();
std::chrono::duration<float> DisplayManager::_delta = std::chrono::duration<float>(0.0f);

DisplayManager::DisplayManager()
{
}

DisplayManager::~DisplayManager()
{
}

GLFWwindow* DisplayManager::createDisplay(bool p_limitFPS)
{
	GLFWwindow *window = glfwCreateWindow(_WIDTH, _HEIGHT, _TITLE, nullptr, nullptr);

	int screen_width, screen_heigth;
	glfwGetFramebufferSize(window, &screen_width, &screen_heigth);

	if (nullptr == window)
	{
		std::cout << "Failed to create GLFW Window" << std::endl;
		glfwTerminate();
	}

	glfwMakeContextCurrent(window);

	if (!p_limitFPS)
	{
		glfwSwapInterval(0);
	}

	glewExperimental = GL_TRUE;

	if (GLEW_OK != glewInit())
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
	}


	glViewport(0, 0, screen_width, screen_heigth);

	DisplayManager::_start = std::chrono::system_clock::now();

	return window;
}

void DisplayManager::updateDisplay()
{
	DisplayManager::_delta = std::chrono::system_clock::now() - _start;
	DisplayManager::_start = std::chrono::system_clock::now();
	std::cout << 1.0f/(std::chrono::duration_cast<std::chrono::milliseconds>(_delta).count() / 1000.0f) << std::endl; //display fps
}

void DisplayManager::closeDisplay()
{
	
}

float DisplayManager::getFrameTimeMiliseconds()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(_delta).count();
}

