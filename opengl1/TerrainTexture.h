
#ifndef TERRAIN_TEXTURE_H
#define TERRAIN_TEXTURE_H

class TerrainTexture
{
public:
	explicit TerrainTexture() { _textureID = 0; };
	explicit TerrainTexture(int p_textureID) : _textureID(p_textureID) {};
	~TerrainTexture();

	int getTextureID() const;

private:
	int _textureID;
};

#endif TERRAIN_TEXTURE_H