#include "Utility.h"

#ifndef RAW_MODEL_H
#define RAW_MODEL_H

class RawModel
{
public:
	explicit RawModel(int p_vaoId, int p_vertexCount)
		: _vaoId(p_vaoId),
		_vertexCount(p_vertexCount) {};
	explicit RawModel(int p_vaoId, int p_vertexCount, std::pair<glm::vec3, float> p_sphere)
		: _vaoId(p_vaoId),
		_vertexCount(p_vertexCount),
		_boundingSphere(p_sphere){};
	explicit RawModel() : RawModel(0, 0) {};
	~RawModel();
	int getVaoID() const;
	int getVertexCount() const;
	std::pair<glm::vec3, float> getBoundingSphere() const;
	void setBoundingSphere(std::pair<glm::vec3, float> p_boundingSphere);

private:
	int _vaoId;
	int _vertexCount;
	std::pair<glm::vec3, float> _boundingSphere;
};

#endif RAW_MODEL_H
