#include "RawModel.h"
#include "SkyboxShader.h"
#include "Loader.h"
#include "Camera.h"

#ifndef SKYBOX_RENDERER_H
#define SKYBOX_RENDERER_H

class SkyboxRenderer
{
public:
	explicit SkyboxRenderer() : 
		_cube(nullptr),
		_texture(0),
		_shader(nullptr) {};
	explicit SkyboxRenderer(Loader * p_loader, glm::fmat4 p_projMatrix); 
	SkyboxRenderer(const SkyboxRenderer & p_other) :
		_cube(p_other.getCube()),
		_texture(p_other.getTexture()),
		_shader(p_other.getShader()) {};
	~SkyboxRenderer();

	SkyboxRenderer & operator=(const SkyboxRenderer & p_other);

	RawModel * getCube() const;
	int getTexture() const;
	SkyboxShader * getShader() const;

	void render(Camera & p_camera);

private:
	static const float _SIZE;
	static const std::vector<const char *> _TEXTURES_FILES;
	static const std::vector<float> _VERTICES;
	static const float _SPEED;

	float _rot = 0.0f;
	RawModel * _cube;
	int _texture;
	SkyboxShader * _shader;
};


#endif SKYBOX_RENDERER_H
