#include "MasterRenderer.h"


const float MasterRenderer::_FOV = 90.0f;
const float MasterRenderer::_NEAR_PLANE = 0.1f;
const float MasterRenderer::_FAR_PLANE = 1000.0f;
const glm::fvec3 MasterRenderer::_SKY_COLOR = glm::fvec3(0.49f, 0.6f, 0.66f);

MasterRenderer::MasterRenderer()
{
	_shader = new StaticShader();
	_terrainShader = new TerrainShader();
	EntityRenderer::enableBackfaceCulling();
	createProjectionMatrix();
	_renderer = new EntityRenderer(_shader, _projectionMatrix, nullptr);
	_terrainRenderer = new TerrainRenderer(_terrainShader, _projectionMatrix);
	_skyboxRenderer = new SkyboxRenderer();
}

MasterRenderer::MasterRenderer(Loader * p_loader)
{
	_shader = new StaticShader();
	_terrainShader = new TerrainShader();
	EntityRenderer::enableBackfaceCulling();
	createProjectionMatrix();
	_renderer = new EntityRenderer(_shader, _projectionMatrix, p_loader);
	_terrainRenderer = new TerrainRenderer(_terrainShader, _projectionMatrix);
	_skyboxRenderer = new SkyboxRenderer(p_loader, _projectionMatrix);
}


MasterRenderer::~MasterRenderer()
{
	delete _renderer;
	delete _terrainRenderer;
	delete _skyboxRenderer;
	delete _shader;
	delete _terrainShader;
}

void MasterRenderer::prepare()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(_SKY_COLOR.r, _SKY_COLOR.g, _SKY_COLOR.b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MasterRenderer::cleanUp()
{
	_shader->cleanUp();
	_terrainShader->cleanUp();
}

void MasterRenderer::render(std::vector<Light> & p_lights, Camera & p_camera, glm::fvec4 & p_clipPlane)
{
	prepare();
	_shader->start();
	_shader->loadSkyColor(_SKY_COLOR.r, _SKY_COLOR.g, _SKY_COLOR.b);
	_shader->loadClipPlane(p_clipPlane);
	_shader->loadLights(p_lights);
	_shader->loadViewMatrix(p_camera);
	_renderer->render(_entities);
	_shader->stop();

	_terrainShader->start();
	_terrainShader->loadSkyColor(_SKY_COLOR.r, _SKY_COLOR.g, _SKY_COLOR.b);
	_terrainShader->loadClipPlane(p_clipPlane);
	_terrainShader->loadLights(p_lights);
	_terrainShader->loadViewMatrix(p_camera);
	_terrainRenderer->render(_terrains);
	_terrainShader->stop();

	_skyboxRenderer->render(p_camera);

	_entities.clear();
	_terrains.clear();
}

void MasterRenderer::processEntity(Entity p_entity)
{
	TexturedModel entityModel = p_entity.getModel();
	int id = entityModel.getRawModel().getVaoID() * 1000 + entityModel.getTexture().getTextureID();

	auto search = _entities.find(id);
	if (search != _entities.end()) 
	{
		std::vector<Entity>* batch = &_entities[id].second;
		batch->push_back(p_entity);
	}
	else 
	{
		std::vector<Entity> newBatch;
		newBatch.push_back(p_entity);
		_entities.insert({ id, {entityModel, newBatch} });
	}
}

void MasterRenderer::processTerrain(Terrain p_terrain)
{
	_terrains.push_back(p_terrain);
}

glm::mat4 MasterRenderer::getProjectionMatrix()
{
	return _projectionMatrix;
}

glm::fvec3 MasterRenderer::getSKY_COLOR()
{
	return _SKY_COLOR;
}

int MasterRenderer::getLIGHT_COUNT()
{
	return _LIGHT_COUNT;
}

float MasterRenderer::getFOV()
{
	return _FOV;
}

float MasterRenderer::getNEAR_PLANE()
{
	return _NEAR_PLANE;
}

float MasterRenderer::getFAR_PLANE()
{
	return _FAR_PLANE;
}

void MasterRenderer::createProjectionMatrix()
{
	float aspectRatio = (float)DisplayManager::getWIDTH() / (float)DisplayManager::getHEIGHT();
	float y_scale = (float)((1.0f / tan(glm::radians(_FOV / 2.0f))) * aspectRatio);
	float x_scale = y_scale / aspectRatio;
	float frustum_length = _FAR_PLANE - _NEAR_PLANE;

	_projectionMatrix = glm::mat4(0.0f);
	_projectionMatrix[0][0] = x_scale;
	_projectionMatrix[1][1] = y_scale;
	_projectionMatrix[2][2] = -((_FAR_PLANE + _NEAR_PLANE) / frustum_length);
	_projectionMatrix[2][3] = -1.0f;
	_projectionMatrix[3][2] = -((2.0f * _NEAR_PLANE * _FAR_PLANE) / frustum_length);
	//_projectionMatrix[3][3] = 0.0f;
}
