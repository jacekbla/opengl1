#version 430

in vec3 position;
out vec3 textureCoords;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 transformMatrix;

void main(void)
{
	vec4 worldPosition = transformMatrix * vec4(position, 1.0);
	gl_Position = projectionMatrix * viewMatrix * worldPosition; 
	textureCoords = position;
}