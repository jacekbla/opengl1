#include "TerrainRenderer.h"

TerrainRenderer::~TerrainRenderer()
{
}

void TerrainRenderer::render(std::vector<Terrain> p_terrains)
{
	for (Terrain terrain : p_terrains)
	{
		prepareTerrain(terrain);
		loadModelMatrix(terrain);
		glDrawElements(GL_TRIANGLES, terrain.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
		unbindTexturedModel();
	}
}

void TerrainRenderer::prepareTerrain(Terrain & p_terrain)
{
	RawModel rawModel = p_terrain.getModel();
	glBindVertexArray(rawModel.getVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	bindTextures(p_terrain);
	_shader->loadShineVariables(1.0f, 0.0f);
}

void TerrainRenderer::unbindTexturedModel()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void TerrainRenderer::loadModelMatrix(Terrain & p_terrain)
{
	glm::mat4 transformMatrix = Maths::createTransformMatrix(glm::fvec3(p_terrain.getX(), 0.0f, p_terrain.getZ()), 0.0f, 0.0f, 0.0f, 1.0f);
	_shader->loadTransformMatrix(transformMatrix);
}

void TerrainRenderer::bindTextures(Terrain & p_terrain)
{
	TerrainTexturePack texturePack = p_terrain.getTexturePack();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturePack.getBackgroundTexture()->getTextureID());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texturePack.getRTexture()->getTextureID());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texturePack.getGTexture()->getTextureID());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texturePack.getBTexture()->getTextureID());
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, p_terrain.getBlendMap().getTextureID());
}
