#include "TerrainTexture.h"

#ifndef TERRAIN_TEXTURE_PACK_H
#define TERRAIN_TEXTURE_PACK_H

class TerrainTexturePack
{
public:
	explicit TerrainTexturePack() :
		_backgroundTexture(nullptr),
		_rTexture(nullptr),
		_gTexture(nullptr),
		_bTexture(nullptr) {};
	explicit TerrainTexturePack(TerrainTexture *p_backgroundTexture,
		TerrainTexture *p_rTexture,
		TerrainTexture *p_gTexture,
		TerrainTexture *p_bTexture) : 
		_backgroundTexture(p_backgroundTexture),
		_rTexture(p_rTexture), 
		_gTexture(p_gTexture), 
		_bTexture(p_bTexture) {};
	TerrainTexturePack(const TerrainTexturePack &p_other) :
		_backgroundTexture(p_other.getBackgroundTexture()),
		_rTexture(p_other.getRTexture()),
		_gTexture(p_other.getGTexture()),
		_bTexture(p_other.getBTexture()) {};
	~TerrainTexturePack();

	TerrainTexturePack & operator=(const TerrainTexturePack &p_other);

	TerrainTexture *getBackgroundTexture() const;
	TerrainTexture *getRTexture() const;
	TerrainTexture *getGTexture() const;
	TerrainTexture *getBTexture() const;



private:
	TerrainTexture *_backgroundTexture;
	TerrainTexture *_rTexture;
	TerrainTexture *_gTexture;
	TerrainTexture *_bTexture;
};

#endif TERRAIN_TEXTURE_PACK_H