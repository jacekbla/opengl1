﻿#version 430 
#define MAX_LIGHTS 16

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;
//instanced rendering
layout(location = 3) in mat4 transformMatrix;


out vec2 vTexCoordsFrag;
out vec3 vSurfaceNormal;
out vec3 vToLightVector[MAX_LIGHTS];
out vec3 vToCameraVector;
out float visibility;

//not instanced
//uniform mat4 transformMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition[MAX_LIGHTS];
uniform float useFakeLighting;
uniform int lightCount;
uniform vec4 plane;

const float density = 0.006;
const float gradient = 4.0;



void main() 
{
	vec4 worldPosition = transformMatrix * vec4(vPosition, 1.0);
	vec4 positionRelativeToCam = viewMatrix * worldPosition;

	gl_ClipDistance[0] = dot(worldPosition, plane);

	gl_Position = projectionMatrix * viewMatrix * worldPosition;
	vTexCoordsFrag = vTexCoords;

	vec3 actualNormal = vNormal;
	if(useFakeLighting > 0.5)
	{
		actualNormal = vec3(0.0, 1.0, 0.0);
	}

	vSurfaceNormal = (transformMatrix * vec4(actualNormal, 0.0)).xyz;
	for(int i = 0; i < lightCount; i++)
	{
		vToLightVector[i] = lightPosition[i] - worldPosition.xyz;
	}
	vToCameraVector = (inverse(viewMatrix) * vec4(0.0, 0.0, 0.0, 1.0)).xyz - worldPosition.xyz;

	float distance = length(positionRelativeToCam.xyz);
	visibility = exp(-pow(distance * density, gradient));
	visibility = clamp(visibility, 0.0, 1.0);
}
