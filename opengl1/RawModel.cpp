#include "RawModel.h"

RawModel::~RawModel()
{
}

int RawModel::getVaoID() const
{
	return _vaoId;
}

int RawModel::getVertexCount() const
{
	return _vertexCount;
}

std::pair<glm::vec3, float> RawModel::getBoundingSphere() const
{
	return _boundingSphere;
}

void RawModel::setBoundingSphere(std::pair<glm::vec3, float> p_boundingSphere)
{
	_boundingSphere = p_boundingSphere;
}
