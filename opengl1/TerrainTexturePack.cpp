#include "TerrainTexturePack.h"



TerrainTexturePack::~TerrainTexturePack()
{
	//deleted in main
	/*delete _backgroundTexture;
	delete _rTexture;
	delete _gTexture;
	delete _bTexture;*/
}

TerrainTexturePack & TerrainTexturePack::operator=(const TerrainTexturePack &p_other)
{
	_backgroundTexture = p_other.getBackgroundTexture();
	_rTexture = p_other.getRTexture();
	_gTexture = p_other.getGTexture();
	_bTexture = p_other.getBTexture();
	return(*this);
}

TerrainTexture * TerrainTexturePack::getBackgroundTexture() const
{
	return _backgroundTexture;
}

TerrainTexture * TerrainTexturePack::getRTexture() const
{
	return _rTexture;
}

TerrainTexture * TerrainTexturePack::getGTexture() const
{
	return _gTexture;
}

TerrainTexture * TerrainTexturePack::getBTexture() const
{
	return _bTexture;
}
