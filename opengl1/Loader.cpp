#include "Loader.h"

struct BitMapFile
{
	int sizeX;
	int sizeY;
	unsigned char *data;
};

// Routine to read a bitmap file.
// Works only for uncompressed bmp files of 24-bit color.
BitMapFile* getBMPData(std::string filename)
{
	BitMapFile *bmp = new BitMapFile;
	unsigned int size, offset, headerSize;

	// Read input file name.
	std::ifstream infile(filename.c_str(), std::ios::binary);

	// Get the starting point of the image data.
	infile.seekg(10);
	infile.read((char *)&offset, 4);

	// Get the header size of the bitmap.
	infile.read((char *)&headerSize, 4);

	// Get width and height values in the bitmap header.
	infile.seekg(18);
	infile.read((char *)&bmp->sizeX, 4);
	infile.read((char *)&bmp->sizeY, 4);

	// Allocate buffer for the image.
	size = bmp->sizeX * bmp->sizeY * 24;
	bmp->data = new unsigned char[size];

	// Read bitmap data.
	infile.seekg(offset);
	infile.read((char *)bmp->data, size);

	// Reverse color from bgr to rgb.
	int temp;
	for (int i = 0; i < size; i += 3)
	{
		temp = bmp->data[i];
		bmp->data[i] = bmp->data[i + 2];
		bmp->data[i + 2] = temp;
	}

	return bmp;
}




Loader::Loader()
{}

Loader::~Loader()
{}

RawModel Loader::loadToVAO(std::vector<float> p_positions, std::vector<float> p_colors, std::vector<float> p_texCoords, std::vector<float> p_normals, std::vector<unsigned int> p_indices)
{
	int vaoID = createVAO();
	bindIndicesBuffer(p_indices);
	storeDataInAtrributeList(0, 3, p_positions);
	storeDataInAtrributeList(1, 3, p_colors);
	storeDataInAtrributeList(2, 2, p_texCoords);
	storeDataInAtrributeList(3, 3, p_normals);
	unbindVAO();

	return RawModel(vaoID, p_indices.size());
}

RawModel Loader::loadToVAO(std::vector<float> p_positions, std::vector<float> p_texCoords, std::vector<float> p_normals, std::vector<unsigned int> p_indices)
{
	int vaoID = createVAO();
	bindIndicesBuffer(p_indices);
	storeDataInAtrributeList(0, 3, p_positions);
	storeDataInAtrributeList(1, 2, p_texCoords);
	storeDataInAtrributeList(2, 3, p_normals);
	unbindVAO();

	return RawModel(vaoID, p_indices.size());
}

RawModel Loader::loadToVAO(std::vector<float> p_positions, int p_dimensions) 
{
	int vaoID = createVAO();
	storeDataInAtrributeList(0, p_dimensions, p_positions);
	unbindVAO();
	return RawModel(vaoID, p_positions.size() / p_dimensions);
}

RawModel Loader::loadWaterToVAO(std::vector<float> p_positions, std::vector<float> p_indicators)
{
	int vaoID = createVAO();
	storeDataInAtrributeList(0, 2, p_positions);
	storeDataInAtrributeList(1, 4, p_indicators);
	unbindVAO();

	return RawModel(vaoID, p_positions.size() / 2);
}

RawModel Loader::loadToVAO_sphere(std::vector<float> p_positions, std::vector<float> p_texCoords, std::vector<float> p_normals, 
	std::vector<unsigned int> p_indices, std::pair<glm::vec3, float> p_sphere)
{
	int vaoID = createVAO();
	bindIndicesBuffer(p_indices);
	storeDataInAtrributeList(0, 3, p_positions);
	storeDataInAtrributeList(1, 2, p_texCoords);
	storeDataInAtrributeList(2, 3, p_normals);
	unbindVAO();

	return RawModel(vaoID, p_indices.size(), p_sphere);
}

void Loader::cleanUp()
{
	glDeleteVertexArrays(_vaos.size(), _vaos.data());
	glDeleteBuffers(_vbos.size(), _vbos.data());
	glDeleteTextures(_textures.size(), _textures.data());
}

GLuint Loader::createrEmptyVBO(int p_floatCount)
{
	unsigned int instanceVBO1;
	glGenBuffers(1, &instanceVBO1);
	//_vbos.push_back(instanceVBO1);
	glBindBuffer(GL_ARRAY_BUFFER, instanceVBO1);
	glBufferData(GL_ARRAY_BUFFER, p_floatCount * sizeof(float), nullptr, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return instanceVBO1;
}

void Loader::addInstancedAttribute(int p_vao, int p_vbo, int p_attrib, int p_dataSize, int p_instancedDataLength, int p_offset)
{
	glBindBuffer(GL_ARRAY_BUFFER, p_vbo);
	//glBindVertexArray(p_vao);
	glVertexAttribPointer(p_attrib, p_dataSize, GL_FLOAT, GL_FALSE, sizeof(float) * p_instancedDataLength, (void*)(p_offset * sizeof(float)));
	glVertexAttribDivisor(p_attrib, 1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);
}

void Loader::updateVBO(int p_vbo, std::vector<float> & p_data)
{
	glBindBuffer(GL_ARRAY_BUFFER, p_vbo);
	glBufferData(GL_ARRAY_BUFFER, p_data.size() * sizeof(float), &p_data[0], GL_STREAM_DRAW);
	//glBufferSubData(,);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLuint Loader::loadTexture(std::string p_fileName)
{
	BitMapFile* image;
	image = getBMPData(p_fileName);
	GLuint texID;
	glGenTextures(1, &texID);
	_textures.push_back(texID);

	glBindTexture(GL_TEXTURE_2D, texID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->sizeX, image->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//mipmapy
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -1.0f);

	////anisotropic filtering
	//if (GL_EXT_texture_filter_anisotropic)
	//{
	//	float max;
	//	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max);
	//	float amount = fmin(16.0f, max);//higher = better, but more expensive
	//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	//}
	//else
	//{
	//	std::cout << "Anisotropic filtering not supported" << std::endl;
	//}

	return texID;
}

GLuint Loader::loadPNGTexture(const char* p_fileName)
{
	stb::image image(p_fileName);
	GLuint texID;
	glGenTextures(1, &texID);
	_textures.push_back(texID);

	glBindTexture(GL_TEXTURE_2D, texID);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width(), image.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
	glGenerateMipmap(GL_TEXTURE_2D);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//mipmapy
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -1.0f);

	//anisotropic filtering
	//if (GL_EXT_texture_filter_anisotropic)
	//{
	//	float max;
	//	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max);
	//	float amount = fmin(16.0f, max);//higher = better, but more expensive
	//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	//}
	//else
	//{
	//	std::cout << "Anisotropic filtering not supported" << std::endl;
	//}

	return texID;
}

GLuint Loader::loadPNGTextureNoAlpha(const char* p_fileName)
{
	stb::image image(p_fileName);
	GLuint texID;
	glGenTextures(1, &texID);
	_textures.push_back(texID);

	glBindTexture(GL_TEXTURE_2D, texID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.width(), image.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, image.data());
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//mipmapy
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -1.0f);

	//anisotropic filtering
	//if (GL_EXT_texture_filter_anisotropic)
	//{
	//	float max;
	//	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max);
	//	float amount = fmin(16.0f, max);//higher = better, but more expensive
	//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	//}
	//else
	//{
	//	std::cout << "Anisotropic filtering not supported" << std::endl;
	//}

	return texID;
}

GLuint Loader::loadCubeMap(std::vector<const char*> p_textureFiles)
{
	GLuint texID; 
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

	for (int i = 0; i < p_textureFiles.size(); i++)
	{
		stb::image data(p_textureFiles[i]);
		//right -> left -> top -> bottom -> back -> front
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, data.width(), data.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data.data());
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	_textures.push_back(texID);

	return texID;
}

GLuint Loader::loadCubeMapNoAlpha(std::vector<const char*> p_textureFiles)
{
	GLuint texID;
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

	for (int i = 0; i < p_textureFiles.size(); i++)
	{
		stb::image data(p_textureFiles[i]);
		//right -> left -> top -> bottom -> back -> front
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, data.width(), data.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, data.data());
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	_textures.push_back(texID);

	return texID;
}

int Loader::createVAO()
{
	GLuint vaoID = 1;
	glGenVertexArrays(1, &vaoID);
	_vaos.push_back(vaoID);
	glBindVertexArray(vaoID);
	
	return vaoID;
}

void Loader::storeDataInAtrributeList(int p_attributeNumber, int p_coordinateSize, std::vector<float> p_data)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	_vbos.push_back(vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, p_data.size() * sizeof(float), p_data.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(p_attributeNumber, p_coordinateSize, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::storeIntDataInAtrributeList(int p_attributeNumber, int p_coordinateSize, std::vector<int> p_data)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	_vbos.push_back(vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, p_data.size() * sizeof(int), p_data.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(p_attributeNumber, p_coordinateSize, GL_INT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::unbindVAO()
{
	glBindVertexArray(0);
}

void Loader::bindIndicesBuffer(std::vector<unsigned int> p_indices)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	_vbos.push_back(vboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, p_indices.size() * sizeof(unsigned int), p_indices.data(), GL_STATIC_DRAW);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}