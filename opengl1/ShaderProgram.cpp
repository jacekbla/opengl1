#include "ShaderProgram.h"



ShaderProgram::ShaderProgram(const char* p_vertexFile, const char* p_fragmentFile)
{
	setShaders(p_vertexFile, p_fragmentFile, _programHandle, _vertexShaderHandle, _fragmentShaderHandle);

	//bindAttributes();
	//getAllUniformLocations();
}


ShaderProgram::~ShaderProgram()
{
}

void ShaderProgram::start()
{
	glUseProgram(_programHandle);
}

void ShaderProgram::stop()
{
	glUseProgram(0);
}

void ShaderProgram::cleanUp()
{
	stop();
	glDetachShader(_programHandle, _vertexShaderHandle);
	glDetachShader(_programHandle, _fragmentShaderHandle);
	glDeleteShader(_vertexShaderHandle);
	glDeleteShader(_fragmentShaderHandle);
	glDeleteProgram(_programHandle);
}

char* ShaderProgram::readShader(const char * p_shaderFile)
{
	FILE* filePointer = fopen(p_shaderFile, "rb");
	char* content = NULL;
	long numVal = 0;

	fseek(filePointer, 0L, SEEK_END);
	numVal = ftell(filePointer);
	fseek(filePointer, 0L, SEEK_SET);
	content = (char*)malloc((numVal + 1) * sizeof(char));
	fread(content, 1, numVal, filePointer);
	content[numVal] = '\0';
	fclose(filePointer);
	return content;
}

void ShaderProgram::setShaders(const char* p_vertex, const char* p_pixel, int &p_programHandle, int &p_vertexShaderHandle, int &p_fragmentShaderHandle)
{
	GLint status = 0;

	p_programHandle = glCreateProgram();
	p_vertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
	p_fragmentShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);

	setShader(p_vertex, p_programHandle, p_vertexShaderHandle);
	setShader(p_pixel, p_programHandle, p_fragmentShaderHandle);

	glLinkProgram(p_programHandle);
	glGetObjectParameterivARB(p_programHandle, GL_OBJECT_LINK_STATUS_ARB, &status);
	if (!status) {
		const int maxInfoLogSize = 2048;
		GLchar infoLog[maxInfoLogSize];
		glGetInfoLogARB(p_programHandle, maxInfoLogSize, NULL, infoLog);
		std::cout << infoLog;
	}
}

void ShaderProgram::setShader(const char* p_file, int &p_programHandle, int &p_shaderHandle)
{
	GLint status = 0;

	char* shader = readShader(p_file);

	glShaderSource(p_shaderHandle, 1, (const char**)&shader, NULL);

	glCompileShader(p_shaderHandle);

	glGetObjectParameterivARB(p_shaderHandle, GL_OBJECT_COMPILE_STATUS_ARB, &status);
	if (!status)
	{
		const int maxInfoLogSize = 2048;
		GLchar infoLog[maxInfoLogSize];
		glGetInfoLogARB(p_shaderHandle, maxInfoLogSize, NULL, infoLog);
		std::cout << infoLog;
	}

	glAttachShader(p_programHandle, p_shaderHandle);
}

void ShaderProgram::bindAttributes() {}

void ShaderProgram::bindAttribute(int attribute, const char* variableName)
{
	glBindAttribLocation(_programHandle, attribute, variableName);
}

void ShaderProgram::getAllUniformLocations() {}

int ShaderProgram::getUniformLocation(const char * p_uniformName)
{
	return glGetUniformLocation(_programHandle, p_uniformName);
}

void ShaderProgram::loadInt(int p_location, int p_value)
{
	glUniform1i(p_location, p_value);
}

void ShaderProgram::loadFloat(int p_location, float p_value)
{
	glUniform1f(p_location, p_value);
}

void ShaderProgram::loadVector(int p_location, glm::vec3 &p_value)
{
	glUniform3f(p_location, p_value.x, p_value.y, p_value.z);
}

void ShaderProgram::loadVector(int p_location, glm::vec4 &p_value)
{
	glUniform4f(p_location, p_value.x, p_value.y, p_value.z, p_value.w);
}

void ShaderProgram::loadVector(int p_location, glm::vec2 & p_value)
{
	glUniform2f(p_location, p_value.x, p_value.y);
}

void ShaderProgram::loadBoolean(int p_location, bool p_value)
{
	glUniform1f(p_location, p_value ? 1.0f : 0.0f);
}

void ShaderProgram::loadMatrix(int p_location, glm::mat4 &p_matrix)
{
	glUniformMatrix4fv(p_location, 1, GL_FALSE, &p_matrix[0][0]);
}
