#include "Utility.h"
#include "DisplayManager.h"
#include "Loader.h"
#include "StaticShader.h"
#include "ModelTexture.h"
#include "TexturedModel.h"
#include "Camera.h"
#include "OBJLoader.h"
#include "Light.h"
#include "MasterRenderer.h"
#include "TerrainTexturePack.h"
#include "Culler.h"

#include "WaterShader.h"
#include "WaterRenderer.h"
#include "WaterTile.h"
#include "WaterFrameBuffers.h"
#include "WaterGenerator.h"



static GLFWwindow *window;

void keyboard(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		exit(EXIT_SUCCESS);
	}
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

int getTerrainIndex(float p_x, float p_z, int p_size)
{
	float s = sqrt(p_size);
	int x_wholes = (int)(0.01f * p_x);
	int z_wholes = (int)(0.01f * p_z);
	if (p_x > 0.0f && p_z > 0.0f)
	{
		return (0.5f * s + (float)x_wholes + 0.5f) * s + z_wholes + 1 - 1;
	}
	if (p_x <= 0.0f && p_z > 0.0f)
	{
		return (0.5f * s + (float)x_wholes - 0.5f) * s + z_wholes + 1 - 1;
	}
	if (p_x > 0.0f && p_z <= 0.0f)
	{
		return (0.5f * s + (float)x_wholes + 0.5f) * s + z_wholes - 1;
	}
	if (p_x <= 0.0f && p_z <= 0.0f)
	{
		return (0.5f * s + (float)x_wholes - 0.5f) * s + z_wholes - 1;
	}
}

void scene1(std::vector<Entity> & everyEntity, std::vector<Entity> & p_entities_lod0, std::vector<Entity> & p_entities_lod1, std::vector<Entity> & p_entities_lod2, std::vector<Entity> & p_entities_lod3,
	std::vector<Terrain> & p_terrains_lod0, std::vector<Terrain> & p_terrains_lod1, std::vector<Terrain> & p_terrains_lod2, std::vector<Terrain> & p_terrains_lod3,
	std::vector<WaterTile> & p_waters, std::vector<Light> & p_lights, Loader * p_loader)
{
	//terrain
	TerrainTexture * backgroundTexture = new TerrainTexture(p_loader->loadPNGTextureNoAlpha("res/grass2.png"));
	TerrainTexture * rTexture = new TerrainTexture(p_loader->loadPNGTextureNoAlpha("res/mud.png"));
	TerrainTexture * gTexture = new TerrainTexture(p_loader->loadPNGTextureNoAlpha("res/dirt.png"));
	TerrainTexture * bTexture = new TerrainTexture(p_loader->loadPNGTextureNoAlpha("res/path.png")); 
	
	TerrainTexturePack * texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
	TerrainTexture * blendMap = new TerrainTexture(p_loader->loadPNGTexture("res/blend_map2.png"));

	int xTilesCount = 4;
	int zTilesCount = 4;
	for (int x = -xTilesCount / 2; x < xTilesCount / 2; x++)
	{
		for (int z = -zTilesCount / 2; z < zTilesCount / 2; z++)
		{
			std::string buf1("res/height_maps4_lod0/height_map");	//!!!
			buf1.append(std::to_string(xTilesCount * (x + xTilesCount / 2) + z + zTilesCount / 2 + 1));
			buf1.append(".png");
			Terrain terrain_lod0(x, z, p_loader, *texturePack, *blendMap, buf1.c_str());
			p_terrains_lod0.push_back(terrain_lod0);

			std::string buf2("res/height_maps4_lod1/height_map");
			buf2.append(std::to_string(xTilesCount * (x + xTilesCount / 2) + z + zTilesCount / 2 + 1));
			buf2.append(".png");
			Terrain terrain_lod1(x, z, p_loader, *texturePack, *blendMap, buf2.c_str());
			p_terrains_lod1.push_back(terrain_lod1);

			std::string buf3("res/height_maps4_lod2/height_map");
			buf3.append(std::to_string(xTilesCount * (x + xTilesCount / 2) + z + zTilesCount / 2 + 1));
			buf3.append(".png");
			Terrain terrain_lod2(x, z, p_loader, *texturePack, *blendMap, buf3.c_str());
			p_terrains_lod2.push_back(terrain_lod2);

			std::string buf4("res/height_maps4_lod3/height_map");
			buf4.append(std::to_string(xTilesCount * (x + xTilesCount / 2) + z + zTilesCount / 2 + 1));
			buf4.append(".png");
			Terrain terrain_lod3(x, z, p_loader, *texturePack, *blendMap, buf4.c_str());
			p_terrains_lod3.push_back(terrain_lod3);
		}
	}

	//entities

	//rocks
	RawModel rock_rawModels_lod0 = loadOBJ("res/rock_lod/rock_lod0.obj", p_loader);
	RawModel rock_rawModels_lod1 = loadOBJ("res/rock_lod/rock_lod1.obj", p_loader);
	RawModel rock_rawModels_lod2 = loadOBJ("res/rock_lod/rock_lod2.obj", p_loader);
	RawModel rock_rawModels_lod3 = loadOBJ("res/rock_lod/rock_lod3.obj", p_loader);
	ModelTexture rock_texture(p_loader->loadPNGTextureNoAlpha("res/rock_texture.png"));
	rock_texture.setShineDamper(20.0f);
	rock_texture.setReflectivity(0.3f);


	TexturedModel rock_texturedModel_lod0(rock_rawModels_lod0, rock_texture);
	TexturedModel rock_texturedModel_lod1(rock_rawModels_lod1, rock_texture);
	TexturedModel rock_texturedModel_lod2(rock_rawModels_lod2, rock_texture);
	TexturedModel rock_texturedModel_lod3(rock_rawModels_lod3, rock_texture);

	//tree
	RawModel tree_rawModel_lod0 = loadOBJ("res/tree_lod/tree_lod0.obj", p_loader);
	RawModel tree_rawModel_lod1 = loadOBJ("res/tree_lod/tree_lod1.obj", p_loader);
	RawModel tree_rawModel_lod2 = loadOBJ("res/tree_lod/tree_lod2.obj", p_loader);
	RawModel tree_rawModel_lod3 = loadOBJ("res/tree_lod/tree_lod3.obj", p_loader);
	ModelTexture tree_texture(p_loader->loadPNGTexture("res/leaf.png"));
	tree_texture.getHasTransparency();
	tree_texture.setShineDamper(20.0f);
	tree_texture.setReflectivity(0.4f);


	TexturedModel tree_texturedModel_lod0(tree_rawModel_lod0, tree_texture);
	TexturedModel tree_texturedModel_lod1(tree_rawModel_lod1, tree_texture);
	TexturedModel tree_texturedModel_lod2(tree_rawModel_lod2, tree_texture);
	TexturedModel tree_texturedModel_lod3(tree_rawModel_lod3, tree_texture);


	//grass
	RawModel grass_rawModel = loadOBJ("res/grass_upsidedown.obj", p_loader);
	ModelTexture grass_texture(p_loader->loadPNGTexture("res/grass_plant.png"));
	grass_texture.setShineDamper(10.0f);
	grass_texture.setReflectivity(0.0f);
	grass_texture.setHasTransparency(true);
	grass_texture.setUseFakeLighting(true);


	TexturedModel grass_texturedModel(grass_rawModel, grass_texture);

	float high_x = 200.0f;
	float low_x = -200.0f;

	float high_z = 200.0f;
	float low_z = -200.0f;
	for (int i = 0; i < 200; i++)
	{
		float ele_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
		float ele_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
		float ele_y = p_terrains_lod0[getTerrainIndex(ele_x, ele_z, p_terrains_lod0.size())].getHeightOfTerrain(ele_x, ele_z);
		float ele_r = 0.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (360.0f - 0.0f)));
		float ele_s = 0.2f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.8f - 0.2f)));

		Entity rock_lod0(rock_texturedModel_lod0, glm::vec3(ele_x, ele_y, ele_z), 0.0f, ele_r, 0.0f, ele_s);
		Entity rock_lod1(rock_texturedModel_lod1, glm::vec3(ele_x, ele_y, ele_z), 0.0f, ele_r, 0.0f, ele_s);
		Entity rock_lod2(rock_texturedModel_lod2, glm::vec3(ele_x, ele_y, ele_z), 0.0f, ele_r, 0.0f, ele_s);
		Entity rock_lod3(rock_texturedModel_lod3, glm::vec3(ele_x, ele_y, ele_z), 0.0f, ele_r, 0.0f, ele_s);
		p_entities_lod0.push_back(rock_lod0);
		p_entities_lod1.push_back(rock_lod1);
		p_entities_lod2.push_back(rock_lod2);
		p_entities_lod3.push_back(rock_lod3);
	}
	everyEntity.push_back(p_entities_lod0.back());
	everyEntity.push_back(p_entities_lod1.back());
	everyEntity.push_back(p_entities_lod2.back());
	everyEntity.push_back(p_entities_lod3.back());

	for (int i = 0; i < 150; i++)
	{
		float tree_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
		float tree_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
		float tree_y = p_terrains_lod0[getTerrainIndex(tree_x, tree_z, p_terrains_lod0.size())].getHeightOfTerrain(tree_x, tree_z);

		float tree_r = 0.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (360.0f - 0.0f)));
		float tree_s = 1.7f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (2.1f - 1.7f)));

		if (tree_y > 0.0f)
		{
			Entity tree_lod0(tree_texturedModel_lod0, glm::vec3(tree_x, tree_y, tree_z), 0.0f, tree_r, 0.0f, tree_s);
			Entity tree_lod1(tree_texturedModel_lod1, glm::vec3(tree_x, tree_y, tree_z), 0.0f, tree_r, 0.0f, tree_s);
			Entity tree_lod2(tree_texturedModel_lod2, glm::vec3(tree_x, tree_y, tree_z), 0.0f, tree_r, 0.0f, tree_s);
			Entity tree_lod3(tree_texturedModel_lod3, glm::vec3(tree_x, tree_y, tree_z), 0.0f, tree_r, 0.0f, tree_s);
			p_entities_lod0.push_back(tree_lod0);
			p_entities_lod1.push_back(tree_lod1);
			p_entities_lod2.push_back(tree_lod2);
			p_entities_lod3.push_back(tree_lod3);
		}
		else
		{
			i--;
		}
	}
	everyEntity.push_back(p_entities_lod0.back());
	everyEntity.push_back(p_entities_lod1.back());
	everyEntity.push_back(p_entities_lod2.back());
	everyEntity.push_back(p_entities_lod3.back());

	for (int i = 0; i < 700; i++)
	{
		float grass_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
		float grass_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
		float grass_y = p_terrains_lod0[getTerrainIndex(grass_x, grass_z, p_terrains_lod0.size())].getHeightOfTerrain(grass_x, grass_z);

		if (grass_y > 0.0f)
		{
			float grass_r = 0.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (360.0f - 0.0f)));
			float grass_s = 0.7f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.1f - 0.7f)));

			Entity grass(grass_texturedModel, glm::vec3(grass_x, grass_y, grass_z), 0.0f, grass_r, 0.0f, grass_s);
			p_entities_lod0.push_back(grass);
			p_entities_lod1.push_back(grass);
			p_entities_lod2.push_back(grass);
			p_entities_lod3.push_back(grass);
		}
		else
		{
			i--;
		}
	}
	everyEntity.push_back(p_entities_lod0.back());
	everyEntity.push_back(p_entities_lod1.back());
	everyEntity.push_back(p_entities_lod2.back());
	everyEntity.push_back(p_entities_lod3.back());


	//lights
	p_lights.push_back(Light(glm::vec3(-1000.0f, 700.0f, 500.0f), glm::vec3(0.8f, 0.8f, 0.8f)));
	float light1_x = 50.0f;
	float light1_z = -64.0f;
	float light1_y = p_terrains_lod0[getTerrainIndex(light1_x, light1_z, p_terrains_lod0.size())].getHeightOfTerrain(light1_x, light1_z) + 1.0f;
	float light2_x = -50.0f;
	float light2_z = -64.0f;
	float light2_y = p_terrains_lod0[getTerrainIndex(light2_x, light2_z, p_terrains_lod0.size())].getHeightOfTerrain(light2_x, light2_z) + 1.0f;
	float light3_x = 7.0f;
	float light3_z = -15.0f;
	float light3_y = p_terrains_lod0[getTerrainIndex(light3_x, light3_z, p_terrains_lod0.size())].getHeightOfTerrain(light3_x, light3_z) + 1.0f;
	p_lights.push_back(
		Light(
			glm::vec3(
				light1_x,
				light1_y + 1.9f,
				light1_z),
			glm::vec3(0.9f, 0.0f, 0.0f),
			glm::fvec3(1.0f, 0.01f, 0.002f)));
	p_lights.push_back(
		Light(
			glm::vec3(
				light2_x,
				light2_y + 1.9f,
				light2_z),
			glm::vec3(0.0f, 0.0f, 0.9f),
			glm::fvec3(1.0f, 0.01f, 0.002f)));
	p_lights.push_back(
		Light(
			glm::vec3(
				light3_x,
				light3_y + 1.9f,
				light3_z),
			glm::vec3(0.0f, 0.9f, 0.0f),
			glm::fvec3(1.0f, 0.01f, 0.002f)));


	//water
	p_waters.push_back(WaterGenerator::generate(400, -200.0f, 0.0f, -200.0f, p_loader));
}

void scene2(std::vector<Entity> & everyEntity, std::vector<Entity> & p_entities_lod0, std::vector<Entity> & p_entities_lod1, std::vector<Entity> & p_entities_lod2, std::vector<Entity> & p_entities_lod3,
	std::vector<Terrain> & p_terrains, std::vector<WaterTile> & p_waters, std::vector<Light> & p_lights, Loader * p_loader)
{
	//elephun
	RawModel elephant_rawModels_lod0 = loadOBJ("res/teapot_lod/teapot_lod0.obj", p_loader);
	RawModel elephant_rawModels_lod1 = loadOBJ("res/teapot_lod/teapot_lod1.obj", p_loader);
	RawModel elephant_rawModels_lod2 = loadOBJ("res/teapot_lod/teapot_lod2.obj", p_loader);
	RawModel elephant_rawModels_lod3 = loadOBJ("res/teapot_lod/teapot_lod3.obj", p_loader);
	ModelTexture elephant_texture(p_loader->loadPNGTexture("res/empty_highres.png"));
	elephant_texture.setShineDamper(20.0f);
	elephant_texture.setReflectivity(1.0f);
	TexturedModel elephant_texturedModel_lod0(elephant_rawModels_lod0, elephant_texture);
	TexturedModel elephant_texturedModel_lod1(elephant_rawModels_lod1, elephant_texture);
	TexturedModel elephant_texturedModel_lod2(elephant_rawModels_lod2, elephant_texture);
	TexturedModel elephant_texturedModel_lod3(elephant_rawModels_lod3, elephant_texture);

	int entityCount = 20000;

	float high_x = 200.0f;
	float low_x = -200.0f;

	float high_z = 200.0f; 
	float low_z = -200.0f;

	float high_y = 100.0f;
	float low_y = -100.0f;
	for (int i = 0; i < entityCount; i++)
	{
		float ele_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
		float ele_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
		float ele_y = low_y/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_y/*HIGH*/ - low_y/*LOW*/)));
		float ele_r1 = 0.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (359.0f - 0.0f)));
		float ele_r2 = 0.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (359.0f - 0.0f)));
		float ele_r3 = 0.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (359.0f - 0.0f)));
		float ele_s = 0.2f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (4.0f - 0.2f)));

		Entity ele_lod0(elephant_texturedModel_lod0, glm::vec3(ele_x, ele_y, ele_z), ele_r1, ele_r2, ele_r3, ele_s);
		Entity ele_lod1(elephant_texturedModel_lod1, glm::vec3(ele_x, ele_y, ele_z), ele_r1, ele_r2, ele_r3, ele_s);
		Entity ele_lod2(elephant_texturedModel_lod2, glm::vec3(ele_x, ele_y, ele_z), ele_r1, ele_r2, ele_r3, ele_s);
		Entity ele_lod3(elephant_texturedModel_lod3, glm::vec3(ele_x, ele_y, ele_z), ele_r1, ele_r2, ele_r3, ele_s);
		p_entities_lod0.push_back(ele_lod0);
		p_entities_lod1.push_back(ele_lod1);
		p_entities_lod2.push_back(ele_lod2);
		p_entities_lod3.push_back(ele_lod3);
	}
	everyEntity.push_back(p_entities_lod0.back());
	everyEntity.push_back(p_entities_lod1.back());
	everyEntity.push_back(p_entities_lod2.back());
	everyEntity.push_back(p_entities_lod3.back());

	//lights
	p_lights.push_back(Light(glm::vec3(1000.0f, 700.0f, -500.0f), glm::vec3(0.9f, 0.9f, 0.9f)));
	float light1_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
	float light1_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
	float light1_y = low_y/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_y/*HIGH*/ - low_y/*LOW*/)));
	float light2_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
	float light2_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
	float light2_y = low_y/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_y/*HIGH*/ - low_y/*LOW*/)));
	float light3_x = low_x/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_x/*HIGH*/ - low_x/*LOW*/)));
	float light3_z = low_z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_z - low_z)));
	float light3_y = low_y/*LOW*/ + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high_y/*HIGH*/ - low_y/*LOW*/)));
}

int main(int argc, char **argv)
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	//glfwWindowHint(GLFW_DOUBLEBUFFER, GL_FALSE); //for unlimited fps

	std::cout << "Which scene?" << std::endl;
	int input;
	std::cin >> input;


	window = DisplayManager::createDisplay(false);
	std::cout << glGetString(GL_VERSION) << std::endl;
	std::cout << glfwGetVersionString() << std::endl;
	std::cout << glewGetString(GLEW_VERSION) << std::endl;
	std::cout << GLM_VERSION_MESSAGE << std::endl;

	Loader* loader = new Loader();
	Camera* camera = new Camera();
	MasterRenderer* masterRenderer = new MasterRenderer(loader);
	WaterFrameBuffers* fbos = new WaterFrameBuffers();
	WaterRenderer* waterRenderer = new WaterRenderer(masterRenderer->getProjectionMatrix(), *fbos);

	std::vector<Entity> entities_lod0;
	std::vector<Entity> entities_lod1;
	std::vector<Entity> entities_lod2;
	std::vector<Entity> entities_lod3;
	std::vector<Terrain> terrainTiles_lod0;
	std::vector<Terrain> terrainTiles_lod1;
	std::vector<Terrain> terrainTiles_lod2;
	std::vector<Terrain> terrainTiles_lod3;
	std::vector<Light> lights;
	std::vector<WaterTile> waterTiles;


	std::vector<Entity> everyEntity;

	if (input == 1)
	{
		scene1(everyEntity, entities_lod0, entities_lod1, entities_lod2, entities_lod3, terrainTiles_lod0, terrainTiles_lod1, terrainTiles_lod2, terrainTiles_lod3, waterTiles, lights, loader);
	}
	else if (input == 2)
	{
		scene2(everyEntity, entities_lod0, entities_lod1, entities_lod2, entities_lod3, terrainTiles_lod0, waterTiles, lights, loader);
	}
	else
	{
		scene1(everyEntity, entities_lod0, entities_lod1, entities_lod2, entities_lod3, terrainTiles_lod0, terrainTiles_lod1, terrainTiles_lod2, terrainTiles_lod3, waterTiles, lights, loader);
	}


	std::string plotBuffer("");


	Culler culler(MasterRenderer::getFOV(), (float)DisplayManager::getWIDTH() / (float)DisplayManager::getHEIGHT(),
		MasterRenderer::getNEAR_PLANE(), MasterRenderer::getFAR_PLANE(), camera);

	glfwSetKeyCallback(window, keyboard);

	for (Entity e : everyEntity)
	{
		e.setPosition(glm::vec3(0.0f, 0.0f, 10000.0f));
		masterRenderer->processEntity(e);
	}
	glm::fvec4 temp0(0.0f);
	masterRenderer->render(lights, *camera, temp0);

	int frameCounter = 0;
	float ftAvg10 = 0.0f;
	float primAvg10 = 0.0f;

	float ftCount = 0.0f;
	float ftMax = 0.0f;
	float ftMin = std::numeric_limits<float>::max();
	float primCount = 0.0f;

	//render
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		/*for (Entity & e : entities)
		{
			e.increaseRotation(3.1f, 2.1f, 1.1f);
			e.increasePosition(1.1f, 2.1f, 1.1f);
		}*/

		camera->move(window);

		//scena dynamiczna
		/*int move = 300;
		if (frameCounter < move)
		{
			camera->moveForward();
		}
		else if (frameCounter < move * 2)
		{
			camera->turnLeft();
		}
		else if (frameCounter < move * 3)
		{
			camera->moveForward();
		}
		else if (frameCounter < move * 4)
		{
			camera->turnRight();
		}
		else if (frameCounter < move * 5)
		{
			camera->turnLeft();
		}
		else if (frameCounter < move * 5.5f)
		{
			camera->turnLeft();
		}
		else if (frameCounter < move * 6)
		{
			camera->moveUp();
			camera->moveForward();
		}
		else if (frameCounter < move * 9)
		{
			camera->moveForward();
		}
		else if (frameCounter < move * 11)
		{
			camera->turnLeft();
		}
		else if (frameCounter < move * 12)
		{
			camera->turnLeft();
		}
		else if (frameCounter < move * 13)
		{
			camera->turnUp();
		}
		else if (frameCounter < move * 14)
		{
			camera->turnDown();
		}
		else if (frameCounter < move * 18)
		{
			camera->moveForward();
			camera->turnLeft();
		}
		else if (frameCounter < move * 19)
		{
			camera->turnDown();
		}
		else if (frameCounter < move * 20)
		{
			camera->moveForward();
		}
		else if (frameCounter < move * 21)
		{
			camera->turnUp();
		}
		else if (frameCounter < move * 22)
		{
			camera->turnUp();
		}
		else if (frameCounter < move * 23)
		{
			camera->turnRight();
		}
		else if (frameCounter < move * 24)
		{
			camera->turnRight();
		}
		else
		{
			break;
		}*/

		int objectCount = 0;
		int vertexCount = 0;
		int triangleCount = 0;

		std::vector<Entity> entities;// = entities_lod0;
		std::vector<Terrain> terrainTiles;// = terrainTiles_lod0;
		//lod stuff
		for (int i = 0; i < entities_lod0.size(); i++)
		{
			float dist = glm::distance(entities_lod0[i].getPosition(), camera->getPosition());
			if (dist < 10.0f)
			{
				entities.push_back(entities_lod0[i]); 
			}
			else if (dist < 80.0f)
			{
				entities.push_back(entities_lod1[i]);
			}
			else if (dist < 150.0f)
			{
				entities.push_back(entities_lod2[i]);
			}
			else if (dist < 500.0f)
			{
				entities.push_back(entities_lod3[i]);
			}
			else
			{
				;
			}
		}

		//frustrum culling
		culler.updateCuller(camera);
		culler.frustrumCulling(entities);

		for (int i = 0; i < terrainTiles_lod0.size(); i++)
		{
			float dist = glm::distance(glm::fvec3(
				terrainTiles_lod0[i].getX() + (Terrain::getSIZE() / 2.0f), 0.0f, 
				terrainTiles_lod0[i].getZ() + (Terrain::getSIZE() / 2.0f)),
				camera->getPosition());

			if (dist < 50.0f)
			{
				terrainTiles.push_back(terrainTiles_lod1[i]);	//!!!!!!!!!!!
				vertexCount += terrainTiles_lod0[i].getModel().getVertexCount();
			}
			else if (dist < 150.0f)
			{
				terrainTiles.push_back(terrainTiles_lod1[i]);
				vertexCount += terrainTiles_lod0[i].getModel().getVertexCount();
			}
			else if (dist < 225.0f)
			{
				terrainTiles.push_back(terrainTiles_lod2[i]);
				vertexCount += terrainTiles_lod0[i].getModel().getVertexCount();
			}
			else
			{
				terrainTiles.push_back(terrainTiles_lod3[i]);
				vertexCount += terrainTiles_lod0[i].getModel().getVertexCount();
			}
		}

		/*for (Terrain t : terrainTiles)
		{
			vertexCount += t.getModel().getVertexCount();
		}*/

		for (Entity e : entities)
		{
			vertexCount += e.getModel().getRawModel().getVertexCount();
		}

		//std::cout << entities.size() << std::endl;
		for(WaterTile t : waterTiles)
		{
			vertexCount += t.getVertexCount();
		}

		triangleCount = vertexCount / 3;
		objectCount = entities.size() + terrainTiles.size() + waterTiles.size();

		std::cout << "Object count:" << objectCount << std::endl;
		std::cout << "Vertex count:" << vertexCount << std::endl;
		std::cout << "Triangle count:" << triangleCount << std::endl << std::endl << std::endl;
		float ft = DisplayManager::getFrameTimeMiliseconds();
		ftAvg10 += ft;
		ftCount += ft;
		if (ft > ftMax)
		{
			ftMax = ft;
		}
		if (ft < ftMin)
		{
			ftMin = ft;
		}

		primAvg10 += triangleCount;
		primCount += triangleCount;

		if (frameCounter % 10 == 0)
		{
			ftAvg10 /= 10.0f;
			primAvg10 /= 10.0f;

			plotBuffer.append(std::to_string(ftAvg10));
			plotBuffer.append(",");
			plotBuffer.append(std::to_string(primAvg10));
			plotBuffer.append("\n");

			ftAvg10 = 0.0f;
			primAvg10 = 0.0f;
		}


		frameCounter++;


		glEnable(GL_CLIP_DISTANCE0);

		if (input == 1)
		{
			//render reflection texture to fbo
			fbos->bindReflectionFrameBuffer();
			float distance = 2 * (camera->getPosition().y - WaterRenderer::getHEIGHT());
			float originalCameraY = camera->getPosition().y;
			camera->setPosition(glm::vec3(camera->getPosition().x, originalCameraY - distance, camera->getPosition().z));
			camera->invertPitch();
			for (Terrain t : terrainTiles)
			{
				masterRenderer->processTerrain(t);
			}

			for (Entity e : entities)
			{
				masterRenderer->processEntity(e);
			}
			glm::fvec4 temp1(0.0f, 1.0f, 0.0f, -WaterRenderer::getHEIGHT() + 0.1f);
			masterRenderer->render(lights, *camera, temp1);
			camera->setPosition(glm::vec3(camera->getPosition().x, originalCameraY, camera->getPosition().z));
			camera->invertPitch();
			fbos->unbindCurrentFrameBuffer();

			//render refraction texture to fbo
			fbos->bindRefractionFrameBuffer();
			for (Terrain t : terrainTiles)
			{
				masterRenderer->processTerrain(t);
			}

			for (Entity e : entities)
			{
				masterRenderer->processEntity(e);
			}
			glm::fvec4 temp2(0.0f, -1.0f, 0.0f, WaterRenderer::getHEIGHT() + 0.5f);
			masterRenderer->render(lights, *camera, temp2);
		}

		//render to screen
		glDisable(GL_CLIP_DISTANCE0);
		fbos->unbindCurrentFrameBuffer();
		for (Terrain t : terrainTiles)
		{
			masterRenderer->processTerrain(t);
		}

		for (Entity e : entities)
		{
			masterRenderer->processEntity(e);
		}
		glm::fvec4 temp3(0.0f, -1.0f, 0.0f, -1.0f);
		masterRenderer->render(lights, *camera, temp3);
		waterRenderer->render(*camera, lights, waterTiles);

		DisplayManager::updateDisplay();
		glfwSwapBuffers(window); //glFlush(); //for unlimited fps
	}

	plotBuffer.append("ft avg: ");
	plotBuffer.append(std::to_string(ftCount/7200.0f));
	plotBuffer.append("\n");
	plotBuffer.append("ft max: ");
	plotBuffer.append(std::to_string(ftMax));
	plotBuffer.append("\n");
	plotBuffer.append("ft min: ");
	plotBuffer.append(std::to_string(ftMin));
	plotBuffer.append("\n");
	plotBuffer.append("prim avg: ");
	plotBuffer.append(std::to_string(primCount/7200.0f));
	plotBuffer.append("\n");

	std::ofstream myfile;
	myfile.open("res/save/dynamic_plot.txt");
	myfile << plotBuffer;
	myfile.close();

	fbos->cleanUp();
	masterRenderer->cleanUp();
	loader->cleanUp();
	delete camera;
	delete masterRenderer;
	delete waterRenderer;
	delete fbos;
	DisplayManager::closeDisplay();

	return EXIT_SUCCESS;
}