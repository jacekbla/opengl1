#include "Utility.h"
#include "Camera.h"
#include "Entity.h"

#ifndef CULLER_H
#define CULLER_H

class Culler
{
public:
	explicit Culler(float p_fov, float p_aspectRatio, float p_nearPlaneDist, float p_farPlaneDist, Camera * p_camera);
	~Culler();

	void updateCuller(Camera * p_camera);
	bool frustrumCulling(std::vector<Entity> & p_entities);
private:
	float _fov;
	float _nearPlaneDist;
	glm::fvec3 _nearPlaneCenter;
	float _farPlaneDist;
	glm::fvec3 _farPlaneCenter;
	glm::fvec3 _cameraPos;
	glm::fvec3 _cameraLookDirection;
	glm::fvec3 _cameraUpDirection;
	glm::fvec3 _cameraRightDirection;

	float nearWidth;
	float nearHeight;
	float farWidth;
	float farHeight;

	//point and normal
	std::pair<glm::vec3, glm::vec3> _planes[6];

	enum {
		TOP = 0, BOTTOM, LEFT,
		RIGHT, NEAR, FAR
	};

	void extractPlanes();
};


#endif CULLER_H