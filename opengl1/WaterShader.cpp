#include "WaterShader.h"

const char* WaterShader::_VERTEX_FILE = "waterVert.glsl";
const char* WaterShader::_FRAGMENT_FILE = "waterFrag.glsl";

WaterShader::~WaterShader()
{
}

void WaterShader::bindAttributes()
{
	ShaderProgram::bindAttribute(0, "vPosition");
	ShaderProgram::bindAttribute(1, "vIndicators");
}

void WaterShader::getAllUniformLocations()
{
	_location_projectionMatrix = ShaderProgram::getUniformLocation("projectionMatrix");
	_location_viewMatrix = ShaderProgram::getUniformLocation("viewMatrix");
	_location_modelMatrix = ShaderProgram::getUniformLocation("modelMatrix");
	_location_reflectionTexture = ShaderProgram::getUniformLocation("reflectionTexture");
	_location_refractionTexture = ShaderProgram::getUniformLocation("refractionTexture");
	_location_cameraPosition = ShaderProgram::getUniformLocation("cameraPosition");
	_location_depthMap = ShaderProgram::getUniformLocation("depthMap");
	_location_height = ShaderProgram::getUniformLocation("height");
	_location_waveTime = ShaderProgram::getUniformLocation("waveTime");
	_location_skyColor = ShaderProgram::getUniformLocation("skyColor");
	_location_lightCount = ShaderProgram::getUniformLocation("lightCount");

	for (int i = 0; i < _MAX_LIGHTS; i++)
	{
		std::string buf1("lightPosition[");
		buf1.append(std::to_string(i));
		buf1.append("]");
		_location_lightPosition[i] = ShaderProgram::getUniformLocation(buf1.c_str());

		std::string buf2("lightColor[");
		buf2.append(std::to_string(i));
		buf2.append("]");
		_location_lightColor[i] = ShaderProgram::getUniformLocation(buf2.c_str());

		std::string buf3("attenuation[");
		buf3.append(std::to_string(i));
		buf3.append("]");
		_location_attenuation[i] = ShaderProgram::getUniformLocation(buf3.c_str());
	}
}

void WaterShader::connectTextureUnits()
{
	ShaderProgram::loadInt(_location_reflectionTexture, 0);
	ShaderProgram::loadInt(_location_refractionTexture, 1);
	ShaderProgram::loadInt(_location_depthMap, 3);
}

void WaterShader::loadProjectionMatrix(glm::mat4 &p_matrix)
{
	ShaderProgram::loadMatrix(_location_projectionMatrix, p_matrix);
}

void WaterShader::loadViewMatrix(Camera &p_camera)
{
	glm::mat4 viewMatrix = Maths::createViewMatrix(p_camera);
	ShaderProgram::loadMatrix(_location_viewMatrix, viewMatrix);
	glm::vec3 pos = p_camera.getPosition();
	ShaderProgram::loadVector(_location_cameraPosition, pos);
}

void WaterShader::loadModelMatrix(glm::mat4 &p_matrix)
{
	ShaderProgram::loadMatrix(_location_modelMatrix, p_matrix);
}

void WaterShader::loadHeight(float p_height)
{
	ShaderProgram::loadFloat(_location_height, p_height);
}

void WaterShader::loadWaveTime(float p_waveTime)
{
	ShaderProgram::loadFloat(_location_waveTime, p_waveTime);
}

void WaterShader::loadLights(std::vector<Light> &p_lights)
{
	const int lightCount = p_lights.size();
	for (int i = 0; i < _MAX_LIGHTS; i++)
	{
		if (i < lightCount)
		{
			ShaderProgram::loadVector(_location_lightPosition[i], p_lights[i].getPostion());
			ShaderProgram::loadVector(_location_lightColor[i], p_lights[i].getColor());
			ShaderProgram::loadVector(_location_attenuation[i], p_lights[i].getAttenuation());
		}
		else
		{
			glm::vec3 temp1(0.0f, 0.0f, 0.0f);
			ShaderProgram::loadVector(_location_lightPosition[i], temp1);
			ShaderProgram::loadVector(_location_lightColor[i], temp1);
			glm::vec3 temp2(1.0f, 0.0f, 0.0f);
			ShaderProgram::loadVector(_location_attenuation[i], temp2);
		}
	}
	ShaderProgram::loadInt(_location_lightCount, lightCount);
}

void WaterShader::loadSkyColor(float p_r, float p_g, float p_b)
{
	glm::vec3 color(p_r, p_g, p_b);
	ShaderProgram::loadVector(_location_skyColor, color);
}
