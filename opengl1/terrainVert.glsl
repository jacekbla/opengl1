﻿#version 430 
#define MAX_LIGHTS 16

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;

out vec2 vTexCoordsFrag;
out vec3 vSurfaceNormal;
out vec3 vToLightVector[MAX_LIGHTS];
out vec3 vToCameraVector;
out float visibility;

uniform mat4 transformMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition[MAX_LIGHTS];
uniform int lightCount;

const float density = 0.006;
const float gradient = 4.0;

uniform vec4 plane;

void main() 
{
	vec4 worldPosition = transformMatrix * vec4(vPosition, 1.0);
	vec4 positionRelativeToCam = viewMatrix * worldPosition;

	gl_ClipDistance[0] = dot(worldPosition, plane);

	gl_Position = projectionMatrix * viewMatrix * worldPosition;
	vTexCoordsFrag = vTexCoords;

	vSurfaceNormal = (transformMatrix * vec4(vNormal, 0.0)).xyz;
	for(int i = 0; i < lightCount; i++)
	{
		vToLightVector[i] = lightPosition[i] - worldPosition.xyz;
	}
	vToCameraVector = (inverse(viewMatrix) * vec4(0.0, 0.0, 0.0, 1.0)).xyz - worldPosition.xyz;

	float distance = length(positionRelativeToCam.xyz);
	visibility = exp(-pow(distance * density, gradient));
	visibility = clamp(visibility, 0.0, 1.0);
}
