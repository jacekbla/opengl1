#include "StaticShader.h"

const char* StaticShader::_VERTEX_FILE = "vert.glsl";
const char* StaticShader::_FRAGMENT_FILE = "frag.glsl";


StaticShader::~StaticShader()
{
}

void StaticShader::bindAttributes()
{
	ShaderProgram::bindAttribute(0, "vPosition");
	ShaderProgram::bindAttribute(1, "vTexCoords");
	ShaderProgram::bindAttribute(2, "vNormal");
	ShaderProgram::bindAttribute(3, "transformMatrix");
}

void StaticShader::getAllUniformLocations()
{
	//not instanced
	//_location_transformMatrix = ShaderProgram::getUniformLocation("transformMatrix");
	_location_projectionMatrix = ShaderProgram::getUniformLocation("projectionMatrix");
	_location_viewMatrix = ShaderProgram::getUniformLocation("viewMatrix");
	_location_shineDamper = ShaderProgram::getUniformLocation("shineDamper");
	_location_reflectivity = ShaderProgram::getUniformLocation("reflectivity");
	_location_useFakeLighting = ShaderProgram::getUniformLocation("useFakeLighting");
	_location_skyColor = ShaderProgram::getUniformLocation("skyColor");
	_location_plane = ShaderProgram::getUniformLocation("plane");
	_location_lightCount = ShaderProgram::getUniformLocation("lightCount");

	for (int i = 0; i < _MAX_LIGHTS; i++)
	{
		std::string buf1("lightPosition[");
		buf1.append(std::to_string(i));
		buf1.append("]");
		_location_lightPosition[i] = ShaderProgram::getUniformLocation(buf1.c_str());
	
		std::string buf2("lightColor[");
		buf2.append(std::to_string(i));
		buf2.append("]");
		_location_lightColor[i] = ShaderProgram::getUniformLocation(buf2.c_str());
		
		std::string buf3("attenuation[");
		buf3.append(std::to_string(i));
		buf3.append("]");
		_location_attenuation[i] = ShaderProgram::getUniformLocation(buf3.c_str());
	}
}

void StaticShader::loadUseFakeLighting(bool p_useFakeLighting)
{
	ShaderProgram::loadBoolean(_location_useFakeLighting, p_useFakeLighting);
}

void StaticShader::loadTransformMatrix(glm::mat4 &p_matrix)
{
	ShaderProgram::loadMatrix(_location_transformMatrix, p_matrix);
}

void StaticShader::loadProjectionMatrix(glm::mat4 &p_matrix)
{
	ShaderProgram::loadMatrix(_location_projectionMatrix, p_matrix);
}

void StaticShader::loadViewMatrix(Camera &p_camera)
{
	glm::mat4 viewMatrix = Maths::createViewMatrix(p_camera);
	ShaderProgram::loadMatrix(_location_viewMatrix, viewMatrix);
}

void StaticShader::loadLights(std::vector<Light> &p_lights)
{
	const int lightCount = p_lights.size();
	for (int i = 0; i < _MAX_LIGHTS; i++)
	{
		if (i < lightCount)
		{
			ShaderProgram::loadVector(_location_lightPosition[i], p_lights[i].getPostion());
			ShaderProgram::loadVector(_location_lightColor[i], p_lights[i].getColor());
			ShaderProgram::loadVector(_location_attenuation[i], p_lights[i].getAttenuation());
		}
		else
		{
			glm::vec3 temp1(0.0f, 0.0f, 0.0f);
			ShaderProgram::loadVector(_location_lightPosition[i], temp1);
			ShaderProgram::loadVector(_location_lightColor[i], temp1);
			glm::vec3 temp2(1.0f, 0.0f, 0.0f);
			ShaderProgram::loadVector(_location_attenuation[i], temp2);
		}
	}
	ShaderProgram::loadInt(_location_lightCount, lightCount);
}

void StaticShader::loadShineVariables(float p_damper, float p_reflectivity)
{
	ShaderProgram::loadFloat(_location_shineDamper, p_damper);
	ShaderProgram::loadFloat(_location_reflectivity, p_reflectivity);
}

void StaticShader::loadClipPlane(glm::fvec4 p_plane)
{
	ShaderProgram::loadVector(_location_plane, p_plane);
}

void StaticShader::loadSkyColor(float p_r, float p_g, float p_b)
{
	glm::vec3 color(p_r, p_g, p_b);
	ShaderProgram::loadVector(_location_skyColor, color);
}
